// Wilkins White
// ECE473 - Microcontrollers Lab 2
// Based on lab2_skel.c by R. Traylor
// 9.25.16

//  HARDWARE SETUP:
//  PORTA is connected to the segments of the LED display. and to the pushbuttons.
//  PORTA.0 corresponds to segment a, PORTA.1 corresponds to segement b, etc.
//  PORTB bits 4-6 go to a,b,c inputs of the 74HC138.
//  PORTB bit 7 goes to the PWM transistor base.

#define F_CPU 16000000 // cpu speed in hertz
#define TRUE 1
#define FALSE 0
#include <avr/io.h>
#include <util/delay.h>

//holds data to be sent to the segments. logic zero turns segment on
uint8_t segment_data[5];

//decimal to 7-segment LED display encodings, logic "0" turns on segment
uint8_t dec_to_7seg[10];


//**********************************************************************************
//                            chk_buttons
//Checks the state of the button number passed to it. It shifts in ones till
//the button is pushed. Function returns a 1 only once per debounced button
//push so a debounce and toggle function can be implemented at the same time.
//Adapted to check all buttons from Ganssel's "Guide to Debouncing"
//Expects active low pushbuttons on PINA port.  Debounce time is determined by
//external loop delay times 12.
//
uint8_t chk_buttons(uint8_t button, uint16_t *state) {
  *state = (*state << 1) | (! bit_is_clear(PINA, button)) | 0xE000;
  if (*state == 0xF000) return 1;
  return 0;
}
//***********************************************************************************

//***********************************************************************************
//                                   segment_sum
//takes a 16-bit binary input value and places the appropriate equivalent 4 digit
//BCD segment code in the array segment_data for display.
//array is loaded at exit as:  |digit3|digit2|colon|digit1|digit0|
void segment_sum(uint16_t sum) {

    if(sum > 9999) sum = 9999;                      // cap sum at 9999

    segment_data[4] = (uint8_t)(sum / 1000);        // thousands
    segment_data[3] = (uint8_t)(sum / 100 % 10);    // hundreds
    segment_data[2] = 0;                            // colon
    segment_data[1] = (uint8_t)(sum / 10  % 10);    // tens
    segment_data[0] = (uint8_t)(sum % 10);          // ones

}//segment_sum
//***********************************************************************************

//***********************************************************************************
int main()
{
    uint8_t segment = 0;
    uint8_t i = 0;
    uint16_t count = 1023;
    uint16_t state[8];

    dec_to_7seg[0] = 0xC0; // 0b11000000
    dec_to_7seg[1] = 0xF9; // 0b11111001
    dec_to_7seg[2] = 0xA4; // 0b10100100
    dec_to_7seg[3] = 0xB0; // 0b10110000
    dec_to_7seg[4] = 0x99; // 0b10011001
    dec_to_7seg[5] = 0x92; // 0b10010010
    dec_to_7seg[6] = 0x82; // 0b10000010
    dec_to_7seg[7] = 0xF8; // 0b11111000
    dec_to_7seg[8] = 0x80; // 0b10000000
    dec_to_7seg[9] = 0x90; // 0b10010000

    //set port bits 1 and 4-7 B as outputs
    DDRB = 0xF0;

    while(1){

        //insert loop delay for debounce
        _delay_ms(2);

        //make PORTA an input port with pullups
        PORTA = 0xFF;
        DDRA  = 0x00;

        //enable tristate buffer for pushbutton switches
        //decoded output 5
        PORTB = (5 << 4);

        //now check each button and increment the count as needed
        for(i=0; i<8; i++)
        {
            if(chk_buttons(i, &state[i])) count += (128 >> i);
        }
        //disable tristate buffer for pushbutton switches
        PORTB = 0x00;

        //bound the segment to 0 - 4
        if(segment > 4) segment = 0;

        //bound the count to 1 - 1023
        if(count > 1023) count = 1;

        //break up the disp_value to 4, BCD digits in the array: call (segsum)
		segment_sum(count);

        //make PORTA an output
		DDRA  = 0xFF;

        //send 7 segment code to LED segments
        //send PORTB the digit to display
        //update digit to display
        PORTA = dec_to_7seg[segment_data[segment]];
        PORTB = (segment << 4);

        segment ++;
        if(segment == 2) segment ++; // skip colon
    }//while
}//main
