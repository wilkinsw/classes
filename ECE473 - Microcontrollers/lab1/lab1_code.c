// Wilkins White
// ECE473 - Microcontrollers Lab 1
// Based on lab1_code.c by R. Traylor
// 9.25.16

//This program increments a binary display of the number of button pushes on switch 
//S0 on the mega128 board.

#include <avr/io.h>
#include <util/delay.h>

//*******************************************************************************
//                            debounce_switch                                  
// Adapted from Ganssel's "Guide to Debouncing"            
// Checks the state of pushbutton S0 It shifts in ones till the button is pushed. 
// Function returns a 1 only once per debounced button push so a debounce and toggle 
// function can be implemented at the same time.
//*******************************************************************************

// I modified this function to be useful for multiple switches by removing the static state
// variable and allowing the user to pass in their own variable for state.
uint8_t debounce_switch(uint8_t button, uint16_t *state) {
  *state = (*state << 1) | (! bit_is_clear(PIND, button)) | 0xE000;
  if (*state == 0xF000) return 1;
  return 0;
}

//*******************************************************************************
// Check switch S0.  When found low for 12 passes of "debounce_switch(), increment
// PORTB.  This will make an incrementing count on the port B LEDS. 
//*******************************************************************************
int main()
{
	uint8_t lsb = 0;
	uint8_t msb = 0;
	uint16_t state0 = 0;
	uint16_t state1 = 0;

	DDRB = 0xFF;  //set port B to all outputs

	while(1){     //do forever
	
		// Use button 0 to increment the count.
		if(debounce_switch(0, &state0))
		{
			lsb++;
			if(lsb > 9)  
			{
				lsb = 0;
				msb++;
			}
			if(msb > 9) msb = 0; 
			
			// since msb and lsb are both 8-bit we can safely use them to set PORTB
			PORTB = (lsb | (msb << 4));
		}
	
		// Use button 1 to decrement the count.
		if(debounce_switch(1, &state1))
		{
			lsb--;
			if(lsb > 9)
			{
				lsb = 9;
				msb--;
			}
			if(msb > 9) msb = 9;
			
			// since msb and lsb are both 8-bit we can safely use them to set PORTB
			PORTB = (lsb | (msb << 4));
		}
	
		_delay_ms(2);	//keep in loop to debounce 24ms
	} //while 
} //main