// Wilkins White
// ECE473 - Microcontrollers Lab 4
// 11.10.16

//  HARDWARE SETUP:
//  PORTA is connected to the segments of the LED display. and to the pushbuttons.

//  PORTB bit 0 selects bargraph
//  PORTB bits 1-3 go to spi
//  PORTB bits 4-6 go to a,b,c inputs of the 74HC138 decoder.
//  PORTB bit 7 (OC2) goes to the PWM transistor base.

//  PORTE bit 0 selects encoder
//  PORTE bit 3 controls volume
//  PORTE bit 7 outputs alarm signal

//  PORTF bit 0 goes to light sensor

// Expected Connections:

// Button board         7-Segment board
// ------------         ---------------
//     sw_com           NC
//     com_lvl          Ground
//     com_en           DEC5
//     J1-J8            A-DP

// 7-segment board      Mega128 board
// ---------------      -------------
//     A-DP             PORTA bit 0-7
//     SEL0             PORTB bit 4
//     SEL1             PORTB bit 5
//     SEL2             PORTB bit 6
//     PWM              PORTB bit 7
//     en               vcc
//     en_n             ground

// Bargraph board       Mega128 board
// --------------       -------------
//     regclk           PORTB bit 0 (ss_n)
//     srclk            PORTB bit 1 (sclk)
//     sdin             PORTB bit 2 (mosi)
//     oe_n             ground
//     sd_out           no connect

// Encoder board        Mega128 board
// -------------        -------------
//     SH/LD            PORTE bit 0
//     SCK              PORTB bit 1 (sclk)
//     SER_IN           no connect
//     SER_OUT          PORTB bit 3 (miso)
//     CLK_INH          ground

// Light Sensor         Mega128 board
// ------------         -------------
//     Out              PORTF bit 0 (adc0)

// Audio Board          Mega128 board
// -----------          -------------
//     Alarm            PORTE bit 7
//     Volume           PORTE bit 3 (OC3A)

// Temp Sensor          Mega128 board
// -----------          -------------
//     Alert            no connect
//     ADDR             no connect
//     SMBCLK           PORTD bit 0 (SCL)
//     SMBDAT           PORTD bit 1 (SDA)

// Mega48 board         Mega128 board
// ------------         -------------
//     TX               PORTD bit 2 (RX)
//     RX               PORTD bit 3 (TX)

// Radio Board          Mega128 board
// -----------          -------------
//

#define F_CPU 16000000UL
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hd44780_driver/hd44780.h"
#include "kellen_music.h"
#include "twi_master.h"
#include "lm73_functions.h"

#define USART_BAUDRATE 9600
//Compute baudvalue at compile time from USART_BAUDRATE and F_CPU
#define BAUDVALUE  ((F_CPU/(USART_BAUDRATE * 16UL)) - 1 )

char lcd_string[32];  //holds two strings to refresh the LCD

//holds data to be sent to the segments. logic zero turns segment on
uint8_t segment_data[5];

//decimal to 7-segment LED display encodings, logic "0" turns on segment
uint8_t dec_to_7seg[14];

//holds button states for debouncing
uint16_t state[8];

//which 7-seg digit is currently lit
uint8_t digit = 0;

// tracks user input mode
uint8_t input_mode = 0;

// tracks clock & alarm seconds
uint32_t seconds = 0;
uint32_t alarm = 0;

// tracks if the colon is lit or not
uint8_t colon_flag = 0;

// tracks 24hr or 12hr mode
// 12hr = 0
// 24hr = 1
uint8_t clock_mode = 1;

// interface flags
uint8_t alarm_set_flag = 0;
uint8_t alarm_sounding = 0;
uint8_t pm_flag = 0;

// tracks how many ticks the knobs have had
int8_t tick_count1 = 0;
int8_t tick_count2 = 0;

// timer for snooze
uint8_t snooze_timer = 0;

// volume
uint8_t volume = 5;

// temperature
uint8_t twi_data[2];
volatile uint16_t local_temperature;
char remote_temperature[20];

//**********************************************************************************
//                            spi_transfer
//
uint8_t spi_transfer(uint8_t byte)
{
    PORTE &= ~(0x01);                   // prime encoder
    PORTE |= 0x01;
    SPDR = byte;                        // do spi transfer
    while(bit_is_clear(SPSR, SPIF)){}   // wait for transfer to complete
    PORTB |= 0x01;                      // refresh bar graph
    PORTB &= ~(0x01);
    return SPDR;                        // return encoder data
}
//**********************************************************************************

//**********************************************************************************
//                            chk_buttons
//Checks the state of the button number passed to it. It shifts in ones till
//the button is pushed. Function returns a 1 only once per debounced button
//push so a debounce and toggle function can be implemented at the same time.
//Adapted to check all buttons from Ganssel's "Guide to Debouncing"
//Expects active low pushbuttons on PINA port.  Debounce time is determined by
//external loop delay times 12.
//
uint8_t chk_buttons(uint8_t button, uint16_t *state) {
    *state = (*state << 1) | (! bit_is_clear(PINA, button)) | 0xE000;
    if (*state == 0xF000) return 1;
    return 0;
}
//***********************************************************************************

//***********************************************************************************
//                                   segment_sum
//takes a 16-bit binary input value and places the appropriate equivalent 4 digit
//BCD segment code in the array segment_data for display.
//array is loaded at exit as:  |digit3|digit2|colon|digit1|digit0|
void segment_sum(uint16_t sum) {

    if(sum > 9999) sum = 9999;                      // cap sum at 9999

    segment_data[4] = (uint8_t)(sum / 1000);        // thousands
    segment_data[3] = (uint8_t)(sum / 100 % 10);    // hundreds
    segment_data[1] = (uint8_t)(sum / 10  % 10);    // tens
    segment_data[0] = (uint8_t)(sum % 10);          // ones

    if(colon_flag)
    {
        if(alarm_set_flag) segment_data[2] = 12;
        else segment_data[2] = 10;
    }
    else
    {
        if(alarm_set_flag) segment_data[2] = 13;
        else segment_data[2] = 11;
    }

}//segment_sum
//***********************************************************************************

//***********************************************************************************
//                            spi_init
//Initalizes the SPI port on the mega128. Does not do any further
//external device specific initalizations.  Sets up SPI to be:
//master mode, clock=clk/2, cycle half phase, low polarity, MSB first
//interrupts disabled, poll SPIF bit in SPSR to check xmit completion
void spi_init(void){
 /* Run this code before attempting to write to the LCD.*/
 DDRF  |= 0x08;  //port F bit 3 is enable for LCD
 PORTF &= 0xF7;  //port F bit 3 is initially low

 DDRB  |= 0x07;  //Turn on SS, MOSI, SCLK
 PORTB |= _BV(PB1);  //port B initalization for SPI, SS_n off
//see: /$install_path/avr/include/avr/iom128.h for bit definitions

 //Master mode, Clock=clk/4, Cycle half phase, Low polarity, MSB first
 SPCR=(1<<SPE) | (1<<MSTR); //enable SPI, clk low initially, rising edge sample
 SPSR=(1<<SPI2X);           //SPI at 2x speed (8 MHz)
 }

//***********************************************************************************

//***********************************************************************************
//                              adc_init
// Initalizes the ADC in free running mode and left-aligned
//
void adc_init(void)
{
    //free running mode, left aligned
    ADMUX |= (1<<REFS0) | (1<<ADLAR);
    ADCSRA |= (1<<ADEN) | (1<<ADFR) | (1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0);
    //start conversion to initialize adc
    ADCSRA |= (1<<ADSC);
}
//***********************************************************************************

//***********************************************************************************
//                              tcnt0_init
//Initalizes timer/counter0 (TCNT0). TCNT0 is running in async mode
//with external 32khz crystal.  Runs in CTC mode with clk/256.
//frequency = (32768)/(255) = 128.5Hz
//
void tcnt0_init(void){
  ASSR  |= (1<<AS0);   //ext osc TOSC
  TIMSK |= (1<<TOIE0); //enable timer/counter0 compare match interrupt
  TCCR0 |= (1<<CS00);  //CTC mode, clk/256 prescale
}
//***********************************************************************************

//***********************************************************************************
//                           timer/counter0 ISR
ISR(TIMER0_OVF_vect)
{
    static uint8_t spi_data;
    static uint8_t knob1, knob2, old_knob1, old_knob2;
    static uint8_t isr_count=0;
    static uint8_t alarm_toggle = 0;
    div_t fp_high, fp_low;

    uint8_t adc_result = 0;

    // Set the timer2 PWM with the ADC input
    adc_result = ADCH;
    OCR2 = 100 + (uint8_t)((adc_result * 150)/255); //100-250

    // Set the timer3 PWM with the volume
    OCR3A = (volume * 10);

    if(isr_count % 8 == 0)
    {
        //for note duration (64th notes)
        beat++;

        refresh_lcd(lcd_string);
        while(bit_is_clear(SPSR, SPIF)){}   // wait for transfer to complete
    }

    isr_count++;
    if((isr_count % 64) == 0)
    {
        colon_flag ^= 1;
        alarm_toggle ^= 1;
    }
    if((isr_count % 128) == 0)
    {
        seconds++;
		
        twi_start_rd(LM73_ADDRESS, twi_data, 2);
        local_temperature = ((twi_data[0] << 8) + twi_data[1]) >> 5;

        fp_high = div(local_temperature, 4); //do division by 205 (204.8 to be exact)
        fp_low = div((fp_high.rem*100), 4); //get fraction into non-fractional form

		sprintf(lcd_string, "Local:  %3d.%02d  Remote: %s", fp_high.quot, fp_low.quot, remote_temperature);

        if(snooze_timer > 0)
        {
            music_off();
            snooze_timer--;
            if(snooze_timer == 0 && alarm_set_flag)
            {
                music_on();
                alarm_sounding = 1;
            }
        }
    }

    // make PORTA an input port with pullups
    PORTA = 0xFF;
    DDRA  = 0x00;

    // enable tristate buffer for pushbutton switches
    PORTB = (5 << 4);

    _delay_us(1); //wait for tristate buffer

    // check for button presses
    if(chk_buttons(7, &state[7])) // toggle alarm
    {
        if(alarm_sounding)
        {
            alarm_sounding = 0;
            snooze_timer = 0;
            music_off();
        }
        else
        {
            alarm_set_flag ^= 1;
        }
    }
    if(chk_buttons(6, &state[6])) // set alarm
    {

        if(input_mode == 1) input_mode = 0;
        else input_mode = 1;
    }
    if(chk_buttons(5, &state[5])) // set clock
    {
        if(input_mode == 2) input_mode = 0;
        else input_mode = 2;
    }
    if(chk_buttons(4, &state[4])) clock_mode ^= 1; // set hr mode
    if(chk_buttons(0, &state[0])) // snooze alarm
    {
        if(alarm_sounding)
        {
            alarm_sounding = 0;
            snooze_timer = 10;
        }
    }

    //disable tristate buffer for pushbutton switches
    PORTB = (digit << 4);

    // send the mode display to the bar graph
    if(alarm_sounding)
    {
        if(alarm_toggle == 1) spi_data = 0xFF;
        else if(alarm_toggle == 0) spi_data = 0x00;
    }
    else spi_data = input_mode | (pm_flag << 7);
    spi_data = spi_transfer(spi_data);

    // store the last knob value and load the new one
    old_knob1 = knob1;
    old_knob2 = knob2;
    knob1 = (spi_data >> 2) & 0b11;
    knob2 = spi_data & 0b11;

    // check to see if knob1 has changed position.
    // only increment/decrement if it has advanced two ticks in the same direction.
    switch(knob1)
    {
        case 0b00:
            if(old_knob1 == 0b01) tick_count1++;
            else if(old_knob1 == 0b10) tick_count1--;
            break;

        case 0b01:
            if(old_knob1 == 0b11) tick_count1++;
            else if(old_knob1 == 0b00) tick_count1--;
            break;

        case 0b11:
            if(old_knob1 == 0b10) tick_count1++;
            else if(old_knob1 == 0b01) tick_count1--;
            break;

        case 0b10:
            if(old_knob1 == 0b00) tick_count1++;
            else if(old_knob1 == 0b11) tick_count1--;
            break;
    }

    // adjust hours based on mode and reset tick_count
    if(tick_count1 >= 2)
    {
        switch(input_mode)
        {
            default:
                input_mode = 0;
            case 0:
                // radio
                break;

            case 1:
                // set alarm
                alarm += 3600; // 1 hr
                break;

            case 2:
                // set clock
                seconds += 3600; // 1 hr
                break;
        }
        tick_count1 = 0;
    }
    else if(tick_count1 <= -2)
    {
        switch(input_mode)
        {
            default:
                input_mode = 0;
            case 0:
                // radio
                break;

            case 1:
                // set alarm
                alarm -= 3600; // 1 hr
                break;

            case 2:
                // set clock
                seconds -= 3600; // 1 hr
                break;
        }
        tick_count1 = 0;
    }

    // check to see if knob1 has changed position.
    // only increment/decrement if it has advanced two ticks in the same direction.
    switch(knob2)
    {
        case 0b00:
            if(old_knob2 == 0b01) tick_count2++;
            else if(old_knob2 == 0b10) tick_count2--;
            break;

        case 0b01:
            if(old_knob2 == 0b11) tick_count2++;
            else if(old_knob2 == 0b00) tick_count2--;
            break;

        case 0b11:
            if(old_knob2 == 0b10) tick_count2++;
            else if(old_knob2 == 0b01) tick_count2--;
            break;

        case 0b10:
            if(old_knob2 == 0b00) tick_count2++;
            else if(old_knob2 == 0b11) tick_count2--;
            break;
    }



    // adjust minutes based on mode and reset tick_count
    if(tick_count2 >= 2)
    {
        switch(input_mode)
        {
            default:
                input_mode = 0;
            case 0:
                // volume
                if(volume < 10) volume++;
                break;

            case 1:
                // set alarm
                alarm += 60; // 1 minute
                break;

            case 2:
                // set clock
                seconds += 60; // 1 minute
                break;
        }
        tick_count2 = 0;
    }
    else if(tick_count2 <= -2)
    {
        switch(input_mode)
        {
            default:
                input_mode = 0;
            case 0:
                // volume
                if(volume != 0) volume--;
                break;

            case 1:
                // set alarm
                alarm -= 60; // 1 minute
                break;

            case 2:
                // set clock
                seconds -= 60; // 1 minute
                break;
        }
        tick_count2 = 0;
    }

}
//***********************************************************************************

/************************************************************************************/
/*                             TIMER1_COMPA                                         */
/*Oscillates pin7, PORTE for alarm tone output                                      */
ISR(TIMER1_COMPA_vect) {
  PORTE ^= ALARM_PIN;      //flips the bit, creating a tone
  if(beat >= max_beat) {   //if we've played the note long enough
    notes++;               //move on to the next note
    play_song(song, notes);//and play it
  }
}
/************************************************************************************/

//***********************************************************************************
//                              tcnt2_init
// Initalizes timer/counter2 (TCNT2) to toggle PB7.
// Interrupt occurs at overflow 0xFF. Prescaling clk/64
//
void tcnt2_init(void)
{
    TCCR2 |= (1 << WGM21) | (1 << WGM20) | (1 << COM21) | (1 << CS20);
}
//***********************************************************************************

//***********************************************************************************
//                              tcnt3_init
// Initalizes timer/counter3 (TCNT3) to toggle PE7.
// No interrupt or prescaling.  Fast 8-bit PWM.
//
void tcnt3_init(void)
{
    TCCR3A |= (1<<WGM30) | (1<<COM3A1);
    TCCR3B |= (1<WGM32) | (1<<CS30);
}
//***********************************************************************************

//***********************************************************************************
//                              seconds_to_clock
// Convert the number of elapsed seconds to an HHmm value
// mil=0 for 12hr, mil=1 for 24hr
//
uint16_t seconds_to_clock(uint32_t s, uint8_t mil)
{
    uint16_t minutes = s / 60;
    uint16_t hours = minutes/60;

    minutes %= 60;

    if(mil) hours %= 24;
    else
    {
        hours = (hours % 12);
        if(hours == 0) hours = 12;
    }

    return (uint16_t)(100*hours + minutes);
}
//***********************************************************************************

//***********************************************************************************
//                              USART1_init
//
void USART1_init()
{
    UBRR1H = (BAUDVALUE >>8 ); //load upper byte of the baud rate into UBRR 
	UBRR1L =  BAUDVALUE;       //load lower byte of the baud rate into UBRR 
    //enable tx and rx, enable rx complete interrupt
    UCSR1B = (1<<RXEN1) | (1<<TXEN1) | (1<<RXCIE1);
    //8 bits per character
    UCSR1C = (1<<UCSZ11) | (1<<UCSZ10);
}
//***********************************************************************************

//***********************************************************************************
//                              USART1_RX ISR
//
ISR(USART1_RX_vect)
{
    static uint8_t string_pos = 0;
    static char buffer[20];
    char inchar = 0;

    inchar = UDR1;
    if(inchar == '!') string_pos = 0;
    else if(inchar == '$')
    {
        buffer[string_pos] = '\0';
        sprintf(remote_temperature, "%s", buffer);
    }
    else
    {
        buffer[string_pos++] = inchar;
    }
}
//***********************************************************************************

//***********************************************************************************
int main()
{
    dec_to_7seg[0] = 0xC0; // 0b11000000
    dec_to_7seg[1] = 0xF9; // 0b11111001
    dec_to_7seg[2] = 0xA4; // 0b10100100
    dec_to_7seg[3] = 0xB0; // 0b10110000
    dec_to_7seg[4] = 0x99; // 0b10011001
    dec_to_7seg[5] = 0x92; // 0b10010010
    dec_to_7seg[6] = 0x82; // 0b10000010
    dec_to_7seg[7] = 0xF8; // 0b11111000
    dec_to_7seg[8] = 0x80; // 0b10000000
    dec_to_7seg[9] = 0x90; // 0b10010000

    dec_to_7seg[10] = 0xFC; // alarm off, colon on
    dec_to_7seg[11] = 0xFF; // alarm off, colon off
    dec_to_7seg[12] = 0xF8; // alarm on, colon on
    dec_to_7seg[13] = 0xFB; // alarm on, colon off

    // set port B & D as outputs
    DDRB = 0xFF;
    DDRD = 0xFF;

    // set portE0 as an output
    PORTE |= 0x01;
    DDRE = 0x01;

    // initialize spi and interrupts
    music_init(); //uses tcnt1
    tcnt0_init();
    tcnt2_init();
    tcnt3_init();
    spi_init();
    lcd_init();
    adc_init();
	init_twi();
	USART1_init();
    sei();

    while(1){
        //set 12hr pm flag if seconds over 43200
        if(clock_mode == 0 && seconds > 43200) pm_flag = 1;
        else pm_flag = 0;

        //bound the seconds to 0-86400
        if(seconds > 86400)
        {
            if(seconds > 100000) seconds = 86400;
            else seconds = 0;
        }

        //bound the alarm to 0-86400
        if(alarm > 86400)
        {
            if(alarm > 100000) alarm = 86400;
            else alarm = 0;
        }

        if (alarm_set_flag && seconds == alarm)
        {
            alarm_sounding = 1;
            music_on();
        }

        //break up the disp_value to 4, BCD digits in the array
        if(input_mode == 1)
        {
            segment_sum(seconds_to_clock(alarm, 1));
        }
        else
        {
            segment_sum(seconds_to_clock(seconds, clock_mode));
        }

        //blank the digit
        PORTA = 0xFF;

        //send PORTB the digit to display
        PORTB = (digit << 4);

        //send 7 segment code to LED segments
        PORTA = dec_to_7seg[segment_data[digit]];

        //make PORTA an output
        DDRA  = 0xFF;

        //increment digit
        digit++;

        //bound the digit to 0 - 4
        digit %= 5;
    }//while
}//main
