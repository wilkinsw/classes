#define F_CPU 8000000UL   //8Mhz clock
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdlib.h>
#include "../lm73_functions.h"
#include "twi_master.h"
#include "uart_functions_m48.h"

#define USART_BAUDRATE 9600
//Compute baudvalue at compile time from USART_BAUDRATE and F_CPU
#define BAUDVALUE  ((F_CPU/(USART_BAUDRATE * 16UL)) - 1 )

uint8_t twi_data[2];
char buffer[20];
volatile uint16_t temperature;
div_t fp_high, fp_low;

int main(void)
{
    uart_init();
    DDRB = 0b00000010;
    DDRC = 0b00000010;
    sei();
    UDR0 = 0x00;
    while(1){     //do forever
        if(PINB == 0x00)
            PORTB |= 0x02;
        else
            PORTB &= 0x00;
        _delay_ms(500);
        twi_start_rd(LM73_ADDRESS, twi_data, 2);
        temperature = ((twi_data[0] << 8) + twi_data[1]) >> 5;

        fp_high = div(temperature, 4); //do division by 205 (204.8 to be exact)
        fp_low = div((fp_high.rem*100), 4); //get fraction into non-fractional form

        sprintf(buffer, "!%3d.%02d$", fp_high.quot, fp_low.quot);
        uart_puts(buffer);
    } //while
} //main
