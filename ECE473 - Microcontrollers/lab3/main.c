// Wilkins White
// ECE473 - Microcontrollers Lab 3
// 9.25.16

//  HARDWARE SETUP:
//  PORTA is connected to the segments of the LED display. and to the pushbuttons.
//  PORTA.0 corresponds to segment a, PORTA.1 corresponds to segement b, etc.
//  PORTB bits 4-6 go to a,b,c inputs of the 74HC138.
//  PORTB bit 7 goes to the PWM transistor base.

// Expected Connections:

// Bargraph board          Mega128 board
// --------------      --------------------
//     reglck           PORTB bit 0 (ss_n)
//     srclk            PORTB bit 1 (sclk)
//     sdin             PORTB bit 2 (mosi)
//     oe_n                   ground
//     gnd2                   ground
//     vdd2                     vcc
//     sd_out               no connect

// Encoder board          Mega128 board
// -------------       --------------------
//     SH/LD            PORTE bit 0
//     SCK              PORTB bit 1 (sclk)
//     SER_IN               no connect
//     SER_OUT          PORTB bit 3 (miso)
//     CLK_INH                ground
//     gnd                    ground
//     vdd                      vcc

#define F_CPU 16000000 // cpu speed in hertz
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

//holds data to be sent to the segments. logic zero turns segment on
uint8_t segment_data[5];

//decimal to 7-segment LED display encodings, logic "0" turns on segment
uint8_t dec_to_7seg[10];

//holds button states for debouncing
uint16_t state[2];

// tracks system mode
uint8_t mode = 0;

// tracks 7-seg count
uint16_t count = 0;

//**********************************************************************************
//                            spi_transfer
//
uint8_t spi_transfer(uint8_t byte)
{
    PORTE = 0x00;                       // prime encoder
    PORTE = 0x01;
    SPDR = byte;                        // do spi transfer
    while(bit_is_clear(SPSR, SPIF)){}   // wait for transfer to complete
    PORTB = 0x01;                       // refresh bar graph
    PORTB = 0x00;
    return SPDR;                        // return encoder data
}
//**********************************************************************************

//**********************************************************************************
//                            chk_buttons
//Checks the state of the button number passed to it. It shifts in ones till
//the button is pushed. Function returns a 1 only once per debounced button
//push so a debounce and toggle function can be implemented at the same time.
//Adapted to check all buttons from Ganssel's "Guide to Debouncing"
//Expects active low pushbuttons on PINA port.  Debounce time is determined by
//external loop delay times 12.
//
uint8_t chk_buttons(uint8_t button, uint16_t *state) {
  *state = (*state << 1) | (! bit_is_clear(PINA, button)) | 0xE000;
  if (*state == 0xF000) return 1;
  return 0;
}
//***********************************************************************************

//***********************************************************************************
//                                   segment_sum
//takes a 16-bit binary input value and places the appropriate equivalent 4 digit
//BCD segment code in the array segment_data for display.
//array is loaded at exit as:  |digit3|digit2|colon|digit1|digit0|
void segment_sum(uint16_t sum) {

    if(sum > 9999) sum = 9999;                      // cap sum at 9999

    segment_data[4] = (uint8_t)(sum / 1000);        // thousands
    segment_data[3] = (uint8_t)(sum / 100 % 10);    // hundreds
    segment_data[2] = 0;                            // colon
    segment_data[1] = (uint8_t)(sum / 10  % 10);    // tens
    segment_data[0] = (uint8_t)(sum % 10);          // ones

}//segment_sum
//***********************************************************************************

//***********************************************************************************
//                            spi_init
//Initalizes the SPI port on the mega128. Does not do any further
//external device specific initalizations.  Sets up SPI to be:
//master mode, clock=clk/2, cycle half phase, low polarity, MSB first
//interrupts disabled, poll SPIF bit in SPSR to check xmit completion
void spi_init(void){
  DDRB   = DDRB | 0x07;           //Turn on SS, MOSI, SCLK
  SPCR  |= (1<<SPE) | (1<<MSTR);  //set up SPI mode
  SPSR  |= (1<<SPI2X);            // double speed operation
 }//spi_init
//***********************************************************************************

//***********************************************************************************
//                              tcnt0_init
//Initalizes timer/counter0 (TCNT0). TCNT0 is running in async mode
//with external 32khz crystal.  Runs in normal mode with no prescaling.
//Interrupt occurs at overflow 0xFF.
//
void tcnt0_init(void){
  ASSR  |= (1<<AS0);   //ext osc TOSC
  TIMSK |= (1<<TOIE0); //enable timer/counter0 overflow interrupt
  TCCR0 |= (1<<CS00);  //normal mode, no prescale
}
//***********************************************************************************

//***********************************************************************************
//                           timer/counter0 ISR
ISR(TIMER0_OVF_vect)
{
    static uint8_t spi_data;
    static uint8_t knob[4];
    static int8_t tick_count = 0;

    uint8_t i = 0;
    uint8_t increment = 0;

    // make PORTA an input port with pullups
    PORTA = 0xFF;
    DDRA  = 0x00;

    // enable tristate buffer for pushbutton switches
    PORTB = (5 << 4);

    _delay_us(1); //wait for tristate buffer
	
	// check for button presses
    if(chk_buttons(7, &state[0])) mode ^= (1 << 0);
    if(chk_buttons(6, &state[1])) mode ^= (1 << 1);

    //disable tristate buffer for pushbutton switches
    PORTB = 0;
	
	// send the mode display to the bar graph
    switch(mode)
    {
        default:
            mode = 0;
        case 0b00:
            spi_data = spi_transfer(0x01); // 0000 0001
            increment = 1;
            break;
        case 0b01:
            spi_data = spi_transfer(0x83); // 1000 0011
            increment = 2;
            break;
        case 0b10:
            spi_data = spi_transfer(0x4F); // 0100 1111
            increment = 4;
            break;
        case 0b11:
            spi_data = spi_transfer(0xC0); // 1100 0000
            increment = 0;
            break;
    }
	
	// store the last knob value and load the new one
    knob[3] = knob[1];
    knob[2] = knob[0];
    knob[1] = (spi_data >> 2) & 0b11;
    knob[0] = spi_data & 0b11;

	// check to see if either knob has changed position.
	// only increment/decrement if it has advanced two ticks in the same direction.
    for(i=0; i<2; i++)
    {
        switch(knob[i])
        {
            case 0b00:
                if(knob[i+2] == 0b01) tick_count++;
                else if(knob[i+2] == 0b10) tick_count--;
                break;

            case 0b01:
                if(knob[i+2] == 0b11) tick_count++;
                else if(knob[i+2] == 0b00) tick_count--;
                break;

            case 0b11:
                if(knob[i+2] == 0b10) tick_count++;
                else if(knob[i+2] == 0b01) tick_count--;
                break;

            case 0b10:
                if(knob[i+2] == 0b00) tick_count++;
                else if(knob[i+2] == 0b11) tick_count--;
                break;
        }
    }

    // adjust count and reset tick_count
    if(tick_count >= 2)
    {
        tick_count = 0;
        count += increment;
    }
    else if(tick_count <= -2)
    {
        tick_count = 0;
        count -= increment;
    }
}
//***********************************************************************************

//***********************************************************************************
int main()
{
    uint8_t digit = 0;

    dec_to_7seg[0] = 0xC0; // 0b11000000
    dec_to_7seg[1] = 0xF9; // 0b11111001
    dec_to_7seg[2] = 0xA4; // 0b10100100
    dec_to_7seg[3] = 0xB0; // 0b10110000
    dec_to_7seg[4] = 0x99; // 0b10011001
    dec_to_7seg[5] = 0x92; // 0b10010010
    dec_to_7seg[6] = 0x82; // 0b10000010
    dec_to_7seg[7] = 0xF8; // 0b11111000
    dec_to_7seg[8] = 0x80; // 0b10000000
    dec_to_7seg[9] = 0x90; // 0b10010000

    // set port B as outputs
    DDRB = 0xFF;

    // set portE0 as an output
    PORTE = 0x01;
    DDRE = 0x01;

    // initialize spi and interrupts
    spi_init();
    tcnt0_init();
    sei();

    while(1){
        // skip colon
        if(digit == 2) digit++;

        //bound the digit to 0 - 4
        if(digit > 4) digit = 0;

        //bound the count to 0 - 1023
        if(count > 1023)
        {
            if(count > 2000) count = 0;
            else count = 1023;
        }

        //break up the disp_value to 4, BCD digits in the array: call (segsum)
        segment_sum(count);

        //disable interrupts to avoid flickering
        cli();

        //blank the digit
        PORTA = 0xFF;

        //send PORTB the digit to display
        PORTB = (digit << 4);

        //send 7 segment code to LED segments
        PORTA = dec_to_7seg[segment_data[digit]];

        //make PORTA an output
        DDRA  = 0xFF;

        //re-enable interrupts
        sei();

        digit++;
    }//while
}//main
