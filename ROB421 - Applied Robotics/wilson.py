__author__ = 'White'

import serial
import math
import SimpleCV
from Tkinter import *
from PIL import ImageTk, Image
import time

use_skew = False

debug = False
if debug:
    print "DEBUG SET"

port = "COM3"

initial_x_offset = -95
initial_y_offset = 160

point_buffer = 20
cycle_time = 500
arduino_time = 1222

rotation_speed = 1.21  # Rad/s
pole_down_time = (arduino_time + cycle_time)  # ms
d_theta = rotation_speed * float(pole_down_time) / 1000

state_align = 0
state_find_pond = 1
state_skew = 2
state_grid = 3
state_mouths = 4
state_offsets = 5
state_red = 6
state_yellow = 7
state_green = 8
state_mark = 9
state_ready = 10

state_start = state_align


def xy_to_polar(x, y):
    x = float(x)
    y = float(y)

    if x == 0:
        x = 0.000000001

    if x > 0 and y >= 0:
        quadrant = 1
    elif x <= 0 < y:
        quadrant = 2
    elif x < 0 and y <= 0:
        quadrant = 3
    else:
        quadrant = 4

    r = math.sqrt(pow(x, 2) + pow(y, 2))
    theta = math.atan(y / x)

    if quadrant == 2:
        theta = theta + math.pi
    elif quadrant == 3:
        theta = theta + math.pi
    elif quadrant == 4:
        theta += 2 * math.pi

    return r, theta


def polar_to_xy(r, theta):
    x = round(r * math.cos(theta), 5)
    y = round(r * math.sin(theta), 5)
    return x, y


class Wilson:
    def __init__(self):
        self.camera = SimpleCV.Camera(prop_set={"width": 1024, "height": 768})
        self.skew_set = False
        self.grid_set = False
        self.pond_found = False
        self.skew_x = None
        self.skew_y = None
        self.pond_area = None
        self.grid_sections_x = None
        self.grid_sections_y = None
        self.red_fish = []
        self.yellow_fish = []
        self.green_fish = []
        self.mouths = []
        self.img = None
        self.pond_img = None
        self.pond_mask1 = None
        self.pond_mask2 = None
        self.red_fish_img = None
        self.yellow_fish_img = None
        self.green_fish_img = None
        self.port = None
        self.big_red = None
        self.big_yellow = None
        self.big_green = None
        self.pond_blob = None
        self.origin = (0, 0)
        self.center = (0, 0)
        self.pond_threshold = -1
        if not debug:
            self.serial_begin(port)

    def reset(self):
        if not debug:
            self.port.write("0,0")
            self.port.write("-2,-2")
        time.sleep(1)
        self.skew_set = False
        self.grid_set = False
        self.pond_found = False
        self.skew_x = None
        self.skew_y = None
        self.pond_area = None
        self.grid_sections_x = None
        self.grid_sections_y = None
        self.red_fish = []
        self.yellow_fish = []
        self.green_fish = []
        self.mouths = []
        self.img = None
        self.pond_img = None
        self.pond_mask1 = None
        self.pond_mask2 = None
        self.red_fish_img = None
        self.yellow_fish_img = None
        self.green_fish_img = None
        self.big_red = None
        self.big_yellow = None
        self.big_green = None
        self.pond_blob = None
        self.origin = (0, 0)
        self.center = (0, 0)

    def serial_begin(self, serial_port, baud=115200):
        if serial_port is None:
            raise RuntimeError("Enter Serial port")
        self.port = serial.Serial(serial_port, baud)

    def move_pole(self, coords, offset=(0, 0)):
        try:
            x = -1 * round(float(coords[0] - self.origin[0] + offset[0]) / float(self.img.width) * self.grid_sections_x)
            y = round(float(coords[1] - self.origin[1] + offset[1]) / float(self.img.height) * self.grid_sections_y)
            self.port.write(str(x) + "," + str(y))
            print "Moving to:", str(x), ",", str(y)
            serial_buffer = ""
            while 1:
                c = self.port.read()
                serial_buffer += c
                if c == '\n':
                    print serial_buffer
                    if "done" in serial_buffer:
                        print "Done!"
                        return
                    else:
                        serial_buffer = ""

        except AttributeError:
            print "No Serial Port!"

    def trigger_pole(self):
        try:
            self.port.write("-9,-9")
            serial_buffer = ""
            while 1:
                c = self.port.read()
                serial_buffer += c
                if c == '\n':
                    print serial_buffer
                    if "done" in serial_buffer:
                        print "Done!"
                        return
                    else:
                        serial_buffer = ""
        except AttributeError:
            print "No Serial Port!"
        print "Fish!"

    def set_skew(self, x_start=0, y_start=0, step=5, strictness=0.04):
        if self.img is None:
            self.take_picture()

        if not self.pond_found:
            self.find_pond()

        print "Running skew adjustment..."

        done_flag = 0
        skew_x = x_start
        skew_y = y_start
        result_blob = None

        while skew_x < 200:
            if done_flag:
                break
            skew_y = 10
            skew_x += step
            while skew_y < skew_x / 2:
                skew_y += step
                """
                result = self.pond_mask2.warp(((0, 0),
                                     (self.img.width, 0),
                                     (self.img.width - skew_x, self.img.height - skew_y),
                                     (skew_x, self.img.height - skew_y)))
                """
                result = self.pond_mask2.warp(((skew_x, skew_y),
                                               (self.img.width - skew_x, skew_y),
                                               (self.img.width, self.img.height),
                                               (0, self.img.height)))

                result_blob = result.findBlobs()[-1]
                if result_blob.isCircle(strictness):
                    done_flag = 1
                    break

        if done_flag:
            print "\t", skew_x, skew_y
            self.skew_x = skew_x
            self.skew_y = skew_y
        else:
            raise RuntimeError('Failed skew adjustment')

        print "Done\n"
        self.pond_area = result_blob.area()
        self.skew_set = True
        self.apply_skew()

    def take_picture(self, name="0image.jpg"):

        print "Taking picture..."
        self.img = self.camera.getImage()  # Clear buffer
        self.img = self.camera.getImage()
        # self.img = self.img.rotate(180)
        self.img.save(name)
        print "Done\n"

        if self.skew_set:
            self.apply_skew()

    def load_image(self, name="0image.png"):

        print "Loading image..."
        self.img = SimpleCV.Image(name)
        print "Done\n"

        if self.skew_set:
            self.apply_skew()

    def find_yellow_fish(self, threshold=70, clean=True, point=None):
        if self.img is None:
            self.take_picture()

        if not self.pond_found:
            self.find_pond()

        print "Finding yellow fish..."
        yellow = self.img.colorDistance(SimpleCV.Color.YELLOW)

        yellow2 = yellow.binarize(threshold)
        yellow_blobs = yellow2.findBlobs()

        if self.pond_blob.contains(yellow_blobs[-1]):
            first = yellow_blobs[-1].area()
            second = yellow_blobs[-2].area()
        elif self.pond_blob.contains(yellow_blobs[-2]):
            first = yellow_blobs[-2].area()
            second = yellow_blobs[-3].area()
        elif self.pond_blob.contains(yellow_blobs[-3]):
            first = yellow_blobs[-3].area()
            second = yellow_blobs[-4].area()
        elif self.pond_blob.contains(yellow_blobs[-4]):
            first = yellow_blobs[-4].area()
            second = yellow_blobs[-5].area()
        else:
            print "No Fish"
            self.yellow_fish = []
            return

        if second < (0.6 * first):
            self.big_yellow = float(second)
        else:
            self.big_yellow = float(first)

        if clean:
            while 1:
                found_flag = 0
                for blob in yellow_blobs:
                    if blob.area() < self.big_yellow * 0.6:
                        found_flag = 1
                        blob.draw(SimpleCV.Color.BLACK, width=10)

                if found_flag == 0:
                    break
                yellow2 = yellow2.applyLayers()
                yellow_blobs = yellow2.findBlobs()

            yellow3 = yellow2.applyLayers()
            self.yellow_fish_img = self.img - yellow3.invert()

            yellow_blobs = self.yellow_fish_img.findBlobs()
            self.yellow_fish = []
            for blob in yellow_blobs:
                if self.pond_blob.contains(blob):
                    x_coord = blob.coordinates()[0]
                    y_coord = blob.coordinates()[1]
                    self.yellow_fish.append((x_coord, y_coord))
                    print "\t(", x_coord, ",", y_coord, ")", blob.area()
        else:
            self.yellow_fish = []
            for blob in yellow_blobs:
                if blob.area() > self.big_yellow * 0.8 and self.pond_blob.contains(blob):
                    x_coord = blob.coordinates()[0]
                    y_coord = blob.coordinates()[1]
                    if point is not None:
                        distance = math.sqrt(pow((x_coord - point[0]), 2) + pow((y_coord - point[1]), 2))
                        if distance < point_buffer:
                            self.clear_fish()
                            self.yellow_fish.append((x_coord, y_coord))
                            return True
                        print "Found fish at point", self.yellow_fish
                    self.yellow_fish.append((x_coord, y_coord))

        print "Done\n"
        return False

    def find_green_fish(self, threshold=50, clean=True, point=None):
        if self.img is None:
            self.take_picture()

        if not self.pond_found:
            self.find_pond()

        print "Finding green fish..."
        green = self.img.colorDistance(SimpleCV.Color.GREEN)

        green2 = green.binarize(threshold)
        green_blobs = green2.findBlobs()

        if self.pond_blob.contains(green_blobs[-1]):
            first = green_blobs[-1].area()
            second = green_blobs[-2].area()
        elif self.pond_blob.contains(green_blobs[-2]):
            first = green_blobs[-2].area()
            second = green_blobs[-3].area()
        elif self.pond_blob.contains(green_blobs[-3]):
            first = green_blobs[-3].area()
            second = green_blobs[-4].area()
        elif self.pond_blob.contains(green_blobs[-4]):
            first = green_blobs[-4].area()
            second = green_blobs[-5].area()
        else:
            print "No Fish"
            self.green_fish = []
            return

        if second < (0.6 * first):
            self.big_green = float(second)
        else:
            self.big_green = float(first)

        if clean:
            while 1:
                found_flag = 0
                for blob in green_blobs:
                    if blob.area() < self.big_green * 0.5:
                        found_flag = 1
                        blob.draw(SimpleCV.Color.BLACK, width=10)

                if found_flag == 0:
                    break
                green2 = green2.applyLayers()
                green_blobs = green2.findBlobs()

            green3 = green2.applyLayers()
            self.green_fish_img = self.img - green3.invert()

            green_blobs = self.green_fish_img.findBlobs()
            self.green_fish = []
            for blob in green_blobs:
                if self.pond_blob.contains(blob):
                    x_coord = blob.coordinates()[0]
                    y_coord = blob.coordinates()[1]
                    self.green_fish.append((x_coord, y_coord))
                    print "\t(", x_coord, ",", y_coord, ")", blob.area()
        else:
            self.green_fish = []
            for blob in green_blobs:
                if blob.area() > self.big_green * 0.8 and self.pond_blob.contains(blob):
                    x_coord = blob.coordinates()[0]
                    y_coord = blob.coordinates()[1]
                    if point is not None:
                        distance = math.sqrt(pow((x_coord - point[0]), 2) + pow((y_coord - point[1]), 2))
                        if distance < point_buffer:
                            self.clear_fish()
                            self.green_fish.append((x_coord, y_coord))
                            print "Found fish at point", self.green_fish
                            return True
                    self.green_fish.append((x_coord, y_coord))
        print "Done\n"
        return False

    def find_red_fish(self, threshold=100, clean=True, point=None):
        if self.img is None:
            self.take_picture()

        if not self.pond_found:
            self.find_pond()

        print "Finding red fish..."
        red = self.img.colorDistance(SimpleCV.Color.RED)

        red2 = red.binarize(threshold)
        red_blobs = red2.findBlobs()

        if self.pond_blob.contains(red_blobs[-1]):
            first = red_blobs[-1].area()
            second = red_blobs[-2].area()
        elif self.pond_blob.contains(red_blobs[-2]):
            first = red_blobs[-2].area()
            second = red_blobs[-3].area()
        elif self.pond_blob.contains(red_blobs[-3]):
            first = red_blobs[-3].area()
            second = red_blobs[-4].area()
        elif self.pond_blob.contains(red_blobs[-4]):
            first = red_blobs[-4].area()
            second = red_blobs[-5].area()
        else:
            print "No Fish"
            self.red_fish = []
            return

        if second < (0.6 * first):
            self.big_red = float(second)
        else:
            self.big_red = float(first)

        if clean:
            while 1:
                found_flag = 0
                for blob in red_blobs:
                    if blob.area() < self.big_red * 0.6:
                        found_flag = 1
                        blob.draw(SimpleCV.Color.BLACK, width=10)

                if found_flag == 0:
                    break
                red2 = red2.applyLayers()
                red_blobs = red2.findBlobs()

            red3 = red2.applyLayers()

            self.red_fish_img = self.img - red3.invert()

            self.red_fish = []
            red_blobs = self.red_fish_img.findBlobs()
            for blob in red_blobs:
                if self.pond_blob.contains(blob):
                    x_coord = blob.coordinates()[0]
                    y_coord = blob.coordinates()[1]
                    self.red_fish.append((x_coord, y_coord))
                    print "\t(", x_coord, ",", y_coord, ")", blob.area()
        else:
            self.red_fish = []
            for blob in red_blobs:
                if blob.area() > self.big_red * 0.8 and self.pond_blob.contains(blob):
                    x_coord = blob.coordinates()[0]
                    y_coord = blob.coordinates()[1]
                    if point is not None:
                        distance = math.sqrt(pow((x_coord - point[0]), 2) + pow((y_coord - point[1]), 2))
                        if distance < point_buffer:
                            self.clear_fish()
                            self.red_fish.append((x_coord, y_coord))
                            print "Found fish at point", self.red_fish
                            return True
                    self.red_fish.append((x_coord, y_coord))

        print "Done\n"
        return False

    def find_pond(self, threshold=None, clean=True):
        if threshold is None:
            threshold = self.pond_threshold

        print "Finding pond..."
        if self.img is None:
            self.take_picture()

        blue = self.img.colorDistance(SimpleCV.Color.BLUE)
        pond = blue.binarize(threshold)

        blobs = pond.findBlobs(minsize=1000)
        self.pond_blob = blobs[-1]

        if clean:
            blobs[-1].draw(SimpleCV.Color.WHITE)

            x = 0
            y = 0
            while 1:
                pond[x, y] = SimpleCV.Color.BLACK
                x += 1
                if x == pond.width:
                    x = 0
                    y += 1
                    if y == pond.height:
                        break

            self.pond_mask1 = pond.applyLayers()
            blobs[-1].drawHoles(SimpleCV.Color.WHITE)
            self.pond_mask2 = pond.applyLayers()
            self.pond_img = self.img + self.pond_mask2.invert()
        else:
            self.pond_img = self.img

        self.pond_found = True
        print "Done\n"

    def print_pond_image(self, filename="pond.jpg"):
        print "Printing pond picture..."
        if not self.pond_found:
            self.find_pond()
        self.pond_img.save(filename)
        print "Done"

    def print_red_fish_image(self, filename="red.jpg"):
        print "Saving", filename
        self.red_fish_img.save(filename)

    def print_yellow_fish_image(self, filename="yellow.jpg"):
        print "Saving", filename
        self.yellow_fish_img.save(filename)

    def print_green_fish_image(self, filename="green.jpg"):
        print "Saving", filename
        self.green_fish_img.save(filename)

    def mark_fish(self, dot_size=10, filename="marked_fish.jpg", draw_arc=False, points=None):
        print "Marking fish on image..."

        self.img.drawLine((self.origin[0], self.img.height),
                          (self.origin[0], 0),
                          color=SimpleCV.Color.BLACK,
                          thickness=3)
        self.img.drawLine((0, self.origin[1]),
                          (self.img.width, self.origin[1]),
                          color=SimpleCV.Color.BLACK,
                          thickness=3)

        if draw_arc:
            for mouth in self.mouths:
                self.img.drawCircle(ctr=(mouth[0], mouth[1]),
                                    color=SimpleCV.Color.WHITE,
                                    rad=dot_size,
                                    thickness=dot_size)

                radius, theta = xy_to_polar(mouth[0] - self.center[0], mouth[1] - self.center[1])
                i = theta - d_theta
                while i < theta:
                    i += 5 / radius
                    x, y = polar_to_xy(radius, i)
                    self.img.drawCircle(ctr=(x + self.center[0], y + self.center[1]),
                                        color=SimpleCV.Color.WHITE,
                                        rad=2,
                                        thickness=2)

        for red in self.red_fish:
            self.img.drawCircle(ctr=(red[0], red[1]),
                                color=SimpleCV.Color.BLACK,
                                rad=dot_size,
                                thickness=dot_size)
            self.img.drawCircle(ctr=(red[0], red[1]),
                                color=SimpleCV.Color.RED,
                                rad=dot_size / 2,
                                thickness=dot_size / 2)

        for yellow in self.yellow_fish:
            self.img.drawCircle(ctr=(yellow[0], yellow[1]),
                                color=SimpleCV.Color.BLACK,
                                rad=dot_size,
                                thickness=dot_size)
            self.img.drawCircle(ctr=(yellow[0], yellow[1]),
                                color=SimpleCV.Color.YELLOW,
                                rad=dot_size / 2,
                                thickness=dot_size / 2)

        for green in self.green_fish:
            self.img.drawCircle(ctr=(green[0], green[1]),
                                color=SimpleCV.Color.BLACK,
                                rad=dot_size,
                                thickness=dot_size)
            self.img.drawCircle(ctr=(green[0], green[1]),
                                color=SimpleCV.Color.GREEN,
                                rad=dot_size / 2,
                                thickness=dot_size / 2)

        self.img.drawCircle(ctr=(self.center[0], self.center[1]),
                            color=SimpleCV.Color.BLACK,
                            rad=dot_size,
                            thickness=dot_size)
        try:
            for point in points:
                self.img.drawCircle(ctr=point,
                                    color=SimpleCV.Color.BLACK,
                                    rad=dot_size,
                                    thickness=dot_size)
        except TypeError:
            pass

        result = self.img.applyLayers()
        result.save(filename)
        print "Done\n"

    def calibrate_grid_from_points(self, north, east, south, west, pond_radius=100):
        print "Calibrating grid from points..."
        print "N:", north, "S:", south
        print "E:", east, "W:", west
        pixel_y = south - north
        pixel_x = east - west

        print "Pond is", pixel_y, "pixels high and ", pixel_x, "pixels wide."

        mm_per_pixel_x = float(pond_radius) * 2 / float(pixel_x)
        mm_per_pixel_y = float(pond_radius) * 2 / float(pixel_y)

        self.grid_sections_x = int(mm_per_pixel_x * float(self.img.width))
        self.grid_sections_y = int(mm_per_pixel_y * float(self.img.height))

        print "Grid sections", self.grid_sections_x, self.grid_sections_y

        self.grid_set = True
        print "Done\n"

    def calibrate_grid_from_pond(self, pond_radius=100):
        if self.img is None:
            self.take_picture()

        if not self.pond_area:
            blue = self.img.colorDistance(SimpleCV.Color.BLUE)
            pond = blue.binarize()

            blobs = pond.findBlobs(minsize=1000)
            self.pond_area = blobs[-1].area()

        print "Calibrating grid from pond..."
        mm_area = math.pi * pond_radius * pond_radius  # mm^2
        print "\tArea of pond (mm):", mm_area
        print "\tArea of pond (px):", self.pond_area
        mm_per_pixel = (mm_area / self.pond_area)  # mm^2 / pixel
        print "\tmm per pixel:", mm_per_pixel
        self.grid_sections_x = math.sqrt(mm_per_pixel * self.img.width * self.img.height)  # mm^2
        self.grid_sections_y = self.grid_sections_x
        print "\tGrid sections:", self.grid_sections_x, self.grid_sections_y

        self.grid_set = True
        print "Done\n"

    def show_grid(self):
        if not self.grid_set:
            raise RuntimeError("Calibrate grid first")

        print "Drawing grid image..."
        i = 1
        while i < self.grid_sections_x:
            if i % 10 == 0:
                self.img.drawLine((int(float(self.img.width) / self.grid_sections_x * i), self.img.height),
                                  (int(float(self.img.width) / self.grid_sections_x * i), 0),
                                  color=SimpleCV.Color.BLACK,
                                  thickness=1)
            """
            else:
                self.img.drawLine((int(float(self.img.width) / self.grid_sections_x * i), self.img.height),
                                  (int(float(self.img.width) / self.grid_sections_x * i), 0),
                                  color=SimpleCV.Color.GRAY,
                                  thickness=1)
                                  """
            i += 1

        i = 1
        while i < self.grid_sections_y:
            if i % 10 == 0:
                self.img.drawLine((self.img.width, int(float(self.img.height) / self.grid_sections_y * i)),
                                  (0, int(float(self.img.height) / self.grid_sections_y * i)),
                                  color=SimpleCV.Color.BLACK,
                                  thickness=1)
            """
            else:
                self.img.drawLine((self.img.width, int(float(self.img.height) / self.grid_sections_y * i)),
                                  (0, int(float(self.img.height) / self.grid_sections_y * i)),
                                  color=SimpleCV.Color.GRAY,
                                  thickness=1)
                                  """
            i += 1

        self.img.applyLayers()
        self.img.save("gridlines.jpg")
        print "Done\n"

    def apply_skew(self):
        if self.img is None:
            self.take_picture()
        else:
            print "Applying Skew..."
            """
            img_warped = self.img.warp(((0, 0),
                                        (self.img.width, 0),
                                        (self.img.width - self.skew_x, self.img.height - self.skew_y),
                                        (self.skew_x, self.img.height - self.skew_y)))
            """
            img_warped = self.img.warp(((self.skew_x, self.skew_y),
                                        (self.img.width - self.skew_x, self.skew_y),
                                        (self.img.width, self.img.height),
                                        (0, self.img.height)))

            # Crop out skew
            self.img = img_warped.crop(x=self.skew_x,
                                       y=self.skew_y,
                                       w=self.img.width - self.skew_x * 2,
                                       h=self.img.height - self.skew_y)

            print "Done\n"

    def clear_fish(self):
        self.red_fish = []
        self.green_fish = []
        self.yellow_fish = []


class App:
    def __init__(self, main):
        self.timer = 0
        self.active_field = -1
        self.reset_flag = False
        self.found_flag = False
        self.coordinates = []
        self.offsets = []
        self.origin = (0, 0)
        self.center = (0, 0)
        self.move_count = 0
        self.current_position = 0
        self.prediction = 0
        self.blue_threshold = 150
        self.red_threshold = 90
        self.yellow_threshold = 140
        self.green_threshold = 70
        main.overrideredirect(True)
        main.geometry("{0}x{1}+0+0".format(main.winfo_screenwidth(), main.winfo_screenheight()))
        self.root = main

        self.w = Wilson()
        self.state = state_start

        self.img = ImageTk.PhotoImage(Image.open("wilson.jpg"))

        self.control_frame = Frame(main)
        self.control_frame.pack(side=LEFT, padx=50)
        self.picture_frame = Frame(main, relief=SUNKEN, bd=2)
        self.picture_frame.pack(side=LEFT)
        self.picture = Label(self.picture_frame, image=self.img)
        self.picture.pack(side=LEFT)

        self.control_top = Frame(self.control_frame)
        self.control_main = Frame(self.control_frame)
        self.control_top.pack(side=TOP)
        self.control_main.pack(side=TOP)

        # Heading
        self.heading = Label(self.control_top, width=20, text="Wilson", font=("Helvetica", 16))
        self.heading.pack()

        if debug:
            self.debug_heading = Label(self.control_top, width=20, text="(DEBUG)", font=("Helvetica", 16))
            self.debug_heading.pack()

        self.heading_spacer = Frame(self.control_top, height=30)
        self.heading_spacer.pack()

        # Start Button
        self.start_button = Button(self.control_main,
                                   command=self.start_button,
                                   width="20",
                                   height="5",
                                   text="Start")
        self.start_button.pack()

        self.quit_button = Button(self.control_main,
                                  command=main.destroy,
                                  width="20",
                                  height="5",
                                  text="Quit")
        self.quit_button.pack()

        # Threshold input
        self.input = Text(self.control_main, height=1, width=5)

        # Button1
        self.button1 = Button(self.control_main,
                              command=self.button1,
                              width="20",
                              height="5")

        # Button2
        self.button2 = Button(self.control_main,
                              command=self.button2,
                              width="20",
                              height="5")

        # Buffer rows
        self.buffer_row1 = Frame(self.control_main, height=20, width=100)
        self.buffer_row2 = Frame(self.control_main, height=20, width=100)

        # Origin
        self.row_origin = Frame(self.control_main, width=100)
        self.label_origin = Label(self.row_origin, text="Origin", width=7)
        self.x_entry_origin = Text(self.row_origin, height=1, width=5)
        self.y_entry_origin = Text(self.row_origin, height=1, width=5)
        self.set_button_origin = Button(self.row_origin,
                                        command=lambda: self.set_button(10),
                                        text="Set")
        self.del_button_origin = Button(self.row_origin,
                                        command=lambda: self.del_button(10),
                                        text="Del")

        # Center
        self.row_center = Frame(self.control_main, width=100)
        self.label_center = Label(self.row_center, text="Center", width=7)
        self.x_entry_center = Text(self.row_center, height=1, width=5)
        self.y_entry_center = Text(self.row_center, height=1, width=5)
        self.set_button_center = Button(self.row_center,
                                        command=lambda: self.set_button(11),
                                        text="Set")
        self.del_button_center = Button(self.row_center,
                                        command=lambda: self.del_button(11),
                                        text="Del")

        # Row 0
        self.row0 = Frame(self.control_main, width=100)
        self.label0 = Label(self.row0, text="Point0", width=7)
        self.x_entry0 = Text(self.row0, height=1, width=5)
        self.y_entry0 = Text(self.row0, height=1, width=5)
        self.set_button0 = Button(self.row0,
                                  command=lambda: self.set_button(0),
                                  text="Set")
        self.del_button0 = Button(self.row0,
                                  command=lambda: self.del_button(0),
                                  text="Del")
        self.test_button0 = Button(self.row0,
                                   command=lambda: self.test_button(0),
                                   text="Test")

        # Row 1
        self.row1 = Frame(self.control_main, width=100)
        self.label1 = Label(self.row1, text="Point1", width=7)
        self.x_entry1 = Text(self.row1, height=1, width=5)
        self.y_entry1 = Text(self.row1, height=1, width=5)
        self.set_button1 = Button(self.row1,
                                  command=lambda: self.set_button(1),
                                  text="Set")
        self.del_button1 = Button(self.row1,
                                  command=lambda: self.del_button(1),
                                  text="Del")
        self.test_button1 = Button(self.row1,
                                   command=lambda: self.test_button(1),
                                   text="Test")

        # Row 2
        self.row2 = Frame(self.control_main, width=100)
        self.label2 = Label(self.row2, text="Point2", width=7)
        self.x_entry2 = Text(self.row2, height=1, width=5)
        self.y_entry2 = Text(self.row2, height=1, width=5)
        self.set_button2 = Button(self.row2,
                                  command=lambda: self.set_button(2),
                                  text="Set")
        self.del_button2 = Button(self.row2,
                                  command=lambda: self.del_button(2),
                                  text="Del")
        self.test_button2 = Button(self.row2,
                                   command=lambda: self.test_button(2),
                                   text="Test")

        # Row 3
        self.row3 = Frame(self.control_main, width=100)
        self.label3 = Label(self.row3, text="Point3", width=7)
        self.x_entry3 = Text(self.row3, height=1, width=5)
        self.y_entry3 = Text(self.row3, height=1, width=5)
        self.set_button3 = Button(self.row3,
                                  command=lambda: self.set_button(3),
                                  text="Set")
        self.del_button3 = Button(self.row3,
                                  command=lambda: self.del_button(3),
                                  text="Del")
        self.test_button3 = Button(self.row3,
                                   command=lambda: self.test_button(3),
                                   text="Test")

        # Row 4
        self.row4 = Frame(self.control_main, width=100)
        self.label4 = Label(self.row4, text="Point4", width=7)
        self.x_entry4 = Text(self.row4, height=1, width=5)
        self.y_entry4 = Text(self.row4, height=1, width=5)
        self.set_button4 = Button(self.row4,
                                  command=lambda: self.set_button(4),
                                  text="Set")
        self.del_button4 = Button(self.row4,
                                  command=lambda: self.del_button(4),
                                  text="Del")
        self.test_button4 = Button(self.row4,
                                   command=lambda: self.test_button(4),
                                   text="Test")

        # Row 5
        self.row5 = Frame(self.control_main, width=100)
        self.label5 = Label(self.row5, text="Point5", width=7)
        self.x_entry5 = Text(self.row5, height=1, width=5)
        self.y_entry5 = Text(self.row5, height=1, width=5)
        self.set_button5 = Button(self.row5,
                                  command=lambda: self.set_button(5),
                                  text="Set")
        self.del_button5 = Button(self.row5,
                                  command=lambda: self.del_button(5),
                                  text="Del")
        self.test_button5 = Button(self.row5,
                                   command=lambda: self.test_button(5),
                                   text="Test")

        # Row 6
        self.row6 = Frame(self.control_main, width=100)
        self.label6 = Label(self.row6, text="Point6", width=7)
        self.x_entry6 = Text(self.row6, height=1, width=5)
        self.y_entry6 = Text(self.row6, height=1, width=5)
        self.set_button6 = Button(self.row6,
                                  command=lambda: self.set_button(6),
                                  text="Set")
        self.del_button6 = Button(self.row6,
                                  command=lambda: self.del_button(6),
                                  text="Del")
        self.test_button6 = Button(self.row6,
                                   command=lambda: self.test_button(6),
                                   text="Test")

        # Row 7
        self.row7 = Frame(self.control_main, width=100)
        self.label7 = Label(self.row7, text="Point7", width=7)
        self.x_entry7 = Text(self.row7, height=1, width=5)
        self.y_entry7 = Text(self.row7, height=1, width=5)
        self.set_button7 = Button(self.row7,
                                  command=lambda: self.set_button(7),
                                  text="Set")
        self.del_button7 = Button(self.row7,
                                  command=lambda: self.del_button(7),
                                  text="Del")
        self.test_button7 = Button(self.row7,
                                   command=lambda: self.test_button(7),
                                   text="Test")

        # Row 8
        self.row8 = Frame(self.control_main, width=100)
        self.label8 = Label(self.row8, text="Point8", width=7)
        self.x_entry8 = Text(self.row8, height=1, width=5)
        self.y_entry8 = Text(self.row8, height=1, width=5)
        self.set_button8 = Button(self.row8,
                                  command=lambda: self.set_button(8),
                                  text="Set")
        self.del_button8 = Button(self.row8,
                                  command=lambda: self.del_button(8),
                                  text="Del")
        self.test_button8 = Button(self.row8,
                                   command=lambda: self.test_button(8),
                                   text="Test")

        # Row 9
        self.row9 = Frame(self.control_main, width=100)
        self.label9 = Label(self.row9, text="Point9", width=7)
        self.x_entry9 = Text(self.row9, height=1, width=5)
        self.y_entry9 = Text(self.row9, height=1, width=5)
        self.set_button9 = Button(self.row9,
                                  command=lambda: self.set_button(9),
                                  text="Set")
        self.del_button9 = Button(self.row9,
                                  command=lambda: self.del_button(9),
                                  text="Del")
        self.test_button9 = Button(self.row9,
                                   command=lambda: self.test_button(9),
                                   text="Test")

        # Done button
        self.buffer_row2 = Frame(self.control_main, height=20, width=100)
        self.done_button = Button(self.control_main, width=7, text="Done", command=self.done_button)
        self.reset_button = Button(self.control_main, width=7, text="Reset", command=self.reset, bg="GREY")

        self.picture.bind("<Button 1>", self.set_coordinates)

    def play(self):
        if self.reset_flag:
            return
        self.root.after(cycle_time, self.play)

        if (time.time() - self.timer) > 10:
            self.found_flag = True
            print "timeout"

        if self.found_flag:
            self.w.trigger_pole()
            self.found_flag = False
            self.reset_flag = True
            self.w.port.write("-11,-11")
            serial_buffer = ""
            while 1:
                c = self.w.port.read()
                serial_buffer += c
                if c == '\n':
                    print serial_buffer
                    if "done" in serial_buffer:
                        print "Done!"
                        break
                    else:
                        serial_buffer = ""
            self.reset_flag = False
            self.timer = time.time()

        if self.move_count < 20:
            if debug:
                self.w.load_image("debug.jpg")
            else:
                self.w.take_picture()

            if self.w.find_red_fish(threshold=self.red_threshold,
                                    clean=False,
                                    point=self.prediction):
                self.found_flag = True
            elif self.w.find_green_fish(threshold=self.green_threshold,
                                        clean=False,
                                        point=self.prediction):
                self.found_flag = True
            elif self.w.find_yellow_fish(threshold=self.yellow_threshold,
                                         clean=False,
                                         point=self.prediction):
                self.found_flag = True
            else:
                self.move_count += 1
                self.w.clear_fish()

            self.w.mark_fish(draw_arc=True, dot_size=point_buffer)
            self.set_image("marked_fish.jpg")
        else:
            self.move_count = 0
            self.current_position += 1
            if self.current_position > len(self.coordinates) - 1:
                self.current_position = 0
            self.w.mouths = [self.coordinates[self.current_position]]
            self.w.move_pole(self.coordinates[self.current_position], self.offsets[self.current_position])
            radius, theta = xy_to_polar(self.coordinates[self.current_position][0] - self.center[0],
                                        self.coordinates[self.current_position][1] - self.center[1])
            x, y = polar_to_xy(radius, theta - d_theta)
            self.prediction = (x + self.center[0], y + self.center[1])

    def button1(self):
        if self.state == state_align:  # Align picture
            # Retake
            print "Retake"
            if debug:
                self.w.load_image("debug.jpg")
                self.set_image("debug.jpg")
            else:
                self.w.take_picture()
                self.set_image("0image.jpg")
        elif self.state == state_find_pond:
            print "Finding pond"
            threshold = int(self.input.get("1.0", END)[:-1])
            try:
                self.w.find_pond(threshold=threshold)
                self.w.print_pond_image()
            except:
                self.heading.config(text="Check Detection\nFAILED.")
                self.set_image("wilson.jpg")
                return
            self.blue_threshold = threshold
            self.w.pond_threshold = threshold
            self.set_image("pond.jpg")
            self.heading.config(text="Check Detection\nSuccess!")
            if not debug:
                #self.button1.config(state=DISABLED)
                self.button2.config(state=ACTIVE)
        elif self.state == state_skew:  # Set Skew
            # Begin Skew Adjustment
            print "Begin Skew Adjustment"
            try:
                if use_skew:
                    self.w.set_skew()
                self.w.print_pond_image()
            except:
                self.heading.config(text="Skew Adjustment\nFAILED.")
                self.set_image("wilson.jpg")
                return
            self.set_image("pond.jpg")
            self.heading.config(text="Skew Adjustment\nSuccess!")
            if not debug:
                self.button1.config(state=DISABLED)
                self.button2.config(state=ACTIVE)
        elif self.state == state_grid:
            print "Nothing"
        elif self.state == state_mouths:
            print "Nothing"
        elif self.state == state_red:
            print "Red Fish"
            try:
                threshold = int(self.input.get("1.0", END)[:-1])
                self.w.find_red_fish(threshold=threshold)
            except:
                self.heading.config(text="Find Red Fish\nFAILED.")
                self.set_image("wilson.jpg")
                return
            self.red_threshold = threshold
            self.heading.config(text="Find Red Fish\nSuccess!")
            self.w.print_red_fish_image()
            self.set_image("red.jpg")
        elif self.state == state_yellow:
            print "Yellow Fish"
            try:
                threshold = int(self.input.get("1.0", END)[:-1])
                self.w.find_yellow_fish(threshold=threshold)
            except:
                self.heading.config(text="Find Yellow Fish\nFAILED.")
                self.set_image("wilson.jpg")
                return
            self.yellow_threshold = threshold
            self.heading.config(text="Find Yellow Fish\nSuccess!")
            self.w.print_yellow_fish_image()
            self.set_image("yellow.jpg")
        elif self.state == state_green:
            print "Green Fish"
            try:
                threshold = int(self.input.get("1.0", END)[:-1])
                self.w.find_green_fish(threshold=threshold)
            except:
                self.heading.config(text="Find Green Fish\nFAILED.")
                self.set_image("wilson.jpg")
                return
            self.green_threshold = threshold
            self.heading.config(text="Find Green Fish\nSuccess!")
            self.w.print_green_fish_image()
            self.set_image("green.jpg")
        elif self.state == state_mark:
            print "Mark Fish"
            self.w.mark_fish(dot_size=point_buffer)
            self.set_image("marked_fish.jpg")
        elif self.state == state_ready:
            serial_buffer = ""
            while 1:
                c = self.w.port.read()
                serial_buffer += c
                if c == '\n':
                    print serial_buffer
                    if "play" in serial_buffer:
                        print "Play!"
                        break
                    else:
                        serial_buffer = ""
            self.button1.config(state=DISABLED)
            self.w.mouths = [self.coordinates[self.current_position]]
            self.w.move_pole(self.coordinates[self.current_position], self.offsets[self.current_position])
            radius, theta = xy_to_polar(self.coordinates[self.current_position][0] - self.center[0],
                                        self.coordinates[self.current_position][1] - self.center[1])
            x, y = polar_to_xy(radius, theta - d_theta)
            self.prediction = (x + self.center[0], y + self.center[1])
            print "Starting point:", self.w.mouths
            print "Prediction", self.prediction
            self.timer = time.time()
            self.play()

    def button2(self):
        if self.state == state_align:
            self.heading.config(text="Check Detection")
            self.button1.config(text="Locate Pond", state=ACTIVE)
            self.input.delete("1.0", END)
            self.input.insert(END, str(self.blue_threshold))

            self.button1.pack_forget()
            self.button2.pack_forget()
            self.buffer_row2.pack_forget()
            self.reset_button.pack_forget()

            self.input.pack()
            self.button1.pack()
            self.button2.pack()
            self.buffer_row2.pack()
            self.reset_button.pack()

            if not debug:
                self.button2.config(state=DISABLED)
            self.state = state_find_pond
        elif self.state == state_find_pond:
            self.heading.config(text="Skew Adjustment")
            self.button1.config(text="Adjust Skew", state=ACTIVE)
            self.input.pack_forget()
            if not debug:
                self.button2.config(state=DISABLED)
            self.state = state_skew
        elif self.state == state_skew:
            self.heading.config(text="Grid Calibration")
            self.button1.config(text="Calibrate Grid", state=ACTIVE)
            if not debug:
                self.button2.config(state=DISABLED)
            self.button1.pack_forget()
            self.button2.pack_forget()
            self.buffer_row2.pack_forget()
            self.reset_button.pack_forget()
            self.pack_grid_calibration()
            self.state = state_grid
        elif self.state == state_grid:
            print "Nothing"
        elif self.state == state_mouths:
            print "Nothing"
        elif self.state == state_red:
            self.heading.config(text="Find Yellow Fish")
            self.button1.config(text="Find Yellow", state=ACTIVE)
            self.input.delete("1.0", END)
            self.input.insert(END, str(self.yellow_threshold))
            self.state = state_yellow
            self.set_image("pond.jpg")
        elif self.state == state_yellow:
            self.heading.config(text="Find Green Fish")
            self.button1.config(text="Find Green", state=ACTIVE)
            self.input.delete("1.0", END)
            self.input.insert(END, str(self.green_threshold))
            self.state = state_green
            self.set_image("pond.jpg")
        elif self.state == state_green:
            self.heading.config(text="Display Found Fish")
            if not debug:
                self.w.port.write("0,0")
                self.w.port.write("-2,-2")
            self.button1.config(text="Mark Fish", state=ACTIVE)
            self.state = state_mark
            self.input.pack_forget()
            self.set_image("gridlines.jpg")
        elif self.state == state_mark:
            self.heading.config(text="Ready")
            self.button1.config(text="Play!", state=ACTIVE)
            self.set_image("wilson.jpg")
            # self.button1.pack_forget()
            self.button2.pack_forget()
            # self.buffer_row2.pack_forget()
            self.state = state_ready

    def reset(self):
        print "Resetting"
        self.reset_flag = True
        self.w.reset()
        self.coordinates = []
        self.heading.config(text="Wilson")
        self.forget_mouth_calibration()
        self.button1.pack_forget()
        self.button2.pack_forget()
        self.reset_button.pack_forget()
        self.input.pack_forget()
        self.start_button.pack()
        self.quit_button.pack()
        self.set_image("wilson.jpg")
        self.state = 0
        self.x_entry0.delete("1.0", END)
        self.y_entry0.delete("1.0", END)
        self.x_entry1.delete("1.0", END)
        self.y_entry1.delete("1.0", END)
        self.x_entry2.delete("1.0", END)
        self.y_entry2.delete("1.0", END)
        self.x_entry3.delete("1.0", END)
        self.y_entry3.delete("1.0", END)
        self.x_entry4.delete("1.0", END)
        self.y_entry4.delete("1.0", END)
        self.x_entry5.delete("1.0", END)
        self.y_entry5.delete("1.0", END)
        self.x_entry6.delete("1.0", END)
        self.y_entry6.delete("1.0", END)
        self.x_entry7.delete("1.0", END)
        self.y_entry7.delete("1.0", END)
        self.x_entry8.delete("1.0", END)
        self.y_entry8.delete("1.0", END)
        self.x_entry9.delete("1.0", END)
        self.y_entry9.delete("1.0", END)
        self.label0.config(text="Point0")
        self.label1.config(text="Point1")
        self.label2.config(text="Point2")
        self.label3.config(text="Point3")
        self.label4.config(text="Point4")
        self.label5.config(text="Point5")
        self.label6.config(text="Point6")
        self.label7.config(text="Point7")
        self.label8.config(text="Point8")
        self.label9.config(text="Point9")
        self.test_button0.pack_forget()
        self.test_button1.pack_forget()
        self.test_button2.pack_forget()
        self.test_button3.pack_forget()
        self.test_button4.pack_forget()
        self.test_button5.pack_forget()
        self.test_button6.pack_forget()
        self.test_button7.pack_forget()
        self.test_button8.pack_forget()
        self.test_button9.pack_forget()

    def start_button(self):
        print "Starting"
        self.reset_flag = False
        if not debug:
            self.w.port.write("0,0")
            self.w.port.write("-2,-2")
        self.heading.config(text="Please Align Camera")
        self.button1.config(text="Retake Picture", state=ACTIVE)
        self.button2.config(text="Continue", state=ACTIVE)
        self.reset_button.config(text="Reset")
        if debug:
            self.w.load_image("debug.jpg")
            self.set_image("debug.jpg")
        else:
            self.w.take_picture()
            self.set_image("0image.jpg")
        self.start_button.pack_forget()
        self.quit_button.pack_forget()
        self.button1.pack()
        self.button2.pack()
        self.buffer_row2.pack()
        self.reset_button.pack()

    def set_image(self, filepath):
        self.img = ImageTk.PhotoImage(Image.open(filepath))
        self.picture.config(image=self.img)

    def pack_grid_calibration(self):
        self.heading.config(text="Grid Calibration")

        self.label0.config(text="North")
        self.label1.config(text="East")
        self.label2.config(text="South")
        self.label3.config(text="West")

        self.row0.pack()
        self.label0.pack(side=LEFT)
        self.x_entry0.pack(side=LEFT)
        self.y_entry0.pack(side=LEFT)
        self.set_button0.pack(side=LEFT)
        self.del_button0.pack(side=LEFT)

        self.row1.pack()
        self.label1.pack(side=LEFT)
        self.x_entry1.pack(side=LEFT)
        self.y_entry1.pack(side=LEFT)
        self.set_button1.pack(side=LEFT)
        self.del_button1.pack(side=LEFT)

        self.row2.pack()
        self.label2.pack(side=LEFT)
        self.x_entry2.pack(side=LEFT)
        self.y_entry2.pack(side=LEFT)
        self.set_button2.pack(side=LEFT)
        self.del_button2.pack(side=LEFT)

        self.row3.pack()
        self.label3.pack(side=LEFT)
        self.x_entry3.pack(side=LEFT)
        self.y_entry3.pack(side=LEFT)
        self.set_button3.pack(side=LEFT)
        self.del_button3.pack(side=LEFT)

        self.buffer_row2.pack()
        self.done_button.pack()
        self.reset_button.pack()

    def forget_grid_calibration(self):
        self.row0.pack_forget()
        self.label0.pack_forget()
        self.x_entry0.pack_forget()
        self.y_entry0.pack_forget()
        self.set_button0.pack_forget()
        self.del_button0.pack_forget()
        self.buffer_row1.pack_forget()

        self.row1.pack_forget()
        self.label1.pack_forget()
        self.x_entry1.pack_forget()
        self.y_entry1.pack_forget()
        self.set_button1.pack_forget()
        self.del_button1.pack_forget()

        self.row2.pack_forget()
        self.label2.pack_forget()
        self.x_entry2.pack_forget()
        self.y_entry2.pack_forget()
        self.set_button2.pack_forget()
        self.del_button2.pack_forget()

        self.row3.pack_forget()
        self.label3.pack_forget()
        self.x_entry3.pack_forget()
        self.y_entry3.pack_forget()
        self.set_button3.pack_forget()
        self.del_button3.pack_forget()

        self.buffer_row2.pack_forget()
        self.done_button.pack_forget()
        self.reset_button.pack_forget()

    def pack_mouth_calibration(self):

        self.heading.config(text="Mouth Calibration")
        self.label0.config(text="Point0")
        self.label1.config(text="Point1")
        self.label2.config(text="Point2")
        self.label3.config(text="Point3")

        self.row_origin.pack()
        self.label_origin.pack(side=LEFT)
        self.x_entry_origin.pack(side=LEFT)
        self.y_entry_origin.pack(side=LEFT)
        self.set_button_origin.pack(side=LEFT)
        self.del_button_origin.pack(side=LEFT)

        self.row_center.pack()
        self.label_center.pack(side=LEFT)
        self.x_entry_center.pack(side=LEFT)
        self.y_entry_center.pack(side=LEFT)
        self.set_button_center.pack(side=LEFT)
        self.del_button_center.pack(side=LEFT)
        self.buffer_row1.pack()

        self.row0.pack()
        self.label0.pack(side=LEFT)
        self.x_entry0.pack(side=LEFT)
        self.y_entry0.pack(side=LEFT)
        self.set_button0.pack(side=LEFT)
        self.del_button0.pack(side=LEFT)

        self.row1.pack()
        self.label1.pack(side=LEFT)
        self.x_entry1.pack(side=LEFT)
        self.y_entry1.pack(side=LEFT)
        self.set_button1.pack(side=LEFT)
        self.del_button1.pack(side=LEFT)

        self.row2.pack()
        self.label2.pack(side=LEFT)
        self.x_entry2.pack(side=LEFT)
        self.y_entry2.pack(side=LEFT)
        self.set_button2.pack(side=LEFT)
        self.del_button2.pack(side=LEFT)

        self.row3.pack()
        self.label3.pack(side=LEFT)
        self.x_entry3.pack(side=LEFT)
        self.y_entry3.pack(side=LEFT)
        self.set_button3.pack(side=LEFT)
        self.del_button3.pack(side=LEFT)

        self.row4.pack()
        self.label4.pack(side=LEFT)
        self.x_entry4.pack(side=LEFT)
        self.y_entry4.pack(side=LEFT)
        self.set_button4.pack(side=LEFT)
        self.del_button4.pack(side=LEFT)

        self.row5.pack()
        self.label5.pack(side=LEFT)
        self.x_entry5.pack(side=LEFT)
        self.y_entry5.pack(side=LEFT)
        self.set_button5.pack(side=LEFT)
        self.del_button5.pack(side=LEFT)

        self.row6.pack()
        self.label6.pack(side=LEFT)
        self.x_entry6.pack(side=LEFT)
        self.y_entry6.pack(side=LEFT)
        self.set_button6.pack(side=LEFT)
        self.del_button6.pack(side=LEFT)

        self.row7.pack()
        self.label7.pack(side=LEFT)
        self.x_entry7.pack(side=LEFT)
        self.y_entry7.pack(side=LEFT)
        self.set_button7.pack(side=LEFT)
        self.del_button7.pack(side=LEFT)

        self.row8.pack()
        self.label8.pack(side=LEFT)
        self.x_entry8.pack(side=LEFT)
        self.y_entry8.pack(side=LEFT)
        self.set_button8.pack(side=LEFT)
        self.del_button8.pack(side=LEFT)

        self.row9.pack()
        self.label9.pack(side=LEFT)
        self.x_entry9.pack(side=LEFT)
        self.y_entry9.pack(side=LEFT)
        self.set_button9.pack(side=LEFT)
        self.del_button9.pack(side=LEFT)

        self.buffer_row2.pack()
        self.done_button.pack()
        self.reset_button.pack()

    def forget_mouth_calibration(self):

        self.row_origin.pack_forget()
        self.label_origin.pack_forget()
        self.x_entry_origin.pack_forget()
        self.y_entry_origin.pack_forget()
        self.set_button_origin.pack_forget()
        self.del_button_origin.pack_forget()

        self.row_center.pack_forget()
        self.label_center.pack_forget()
        self.x_entry_center.pack_forget()
        self.y_entry_center.pack_forget()
        self.set_button_center.pack_forget()
        self.del_button_center.pack_forget()
        self.buffer_row1.pack()

        self.row0.pack_forget()
        self.label0.pack_forget()
        self.x_entry0.pack_forget()
        self.y_entry0.pack_forget()
        self.set_button0.pack_forget()
        self.del_button0.pack_forget()

        self.row1.pack_forget()
        self.label1.pack_forget()
        self.x_entry1.pack_forget()
        self.y_entry1.pack_forget()
        self.set_button1.pack_forget()
        self.del_button1.pack_forget()

        self.row2.pack_forget()
        self.label2.pack_forget()
        self.x_entry2.pack_forget()
        self.y_entry2.pack_forget()
        self.set_button2.pack_forget()
        self.del_button2.pack_forget()

        self.row3.pack_forget()
        self.label3.pack_forget()
        self.x_entry3.pack_forget()
        self.y_entry3.pack_forget()
        self.set_button3.pack_forget()
        self.del_button3.pack_forget()

        self.row4.pack_forget()
        self.label4.pack_forget()
        self.x_entry4.pack_forget()
        self.y_entry4.pack_forget()
        self.set_button4.pack_forget()
        self.del_button4.pack_forget()

        self.row5.pack_forget()
        self.label5.pack_forget()
        self.x_entry5.pack_forget()
        self.y_entry5.pack_forget()
        self.set_button5.pack_forget()
        self.del_button5.pack_forget()

        self.row6.pack_forget()
        self.label6.pack_forget()
        self.x_entry6.pack_forget()
        self.y_entry6.pack_forget()
        self.set_button6.pack_forget()
        self.del_button6.pack_forget()

        self.row7.pack_forget()
        self.label7.pack_forget()
        self.x_entry7.pack_forget()
        self.y_entry7.pack_forget()
        self.set_button7.pack_forget()
        self.del_button7.pack_forget()

        self.row8.pack_forget()
        self.label8.pack_forget()
        self.x_entry8.pack_forget()
        self.y_entry8.pack_forget()
        self.set_button8.pack_forget()
        self.del_button8.pack_forget()

        self.row9.pack_forget()
        self.label9.pack_forget()
        self.x_entry9.pack_forget()
        self.y_entry9.pack_forget()
        self.set_button9.pack_forget()
        self.del_button9.pack_forget()

        self.buffer_row2.pack_forget()
        self.done_button.pack_forget()
        self.reset_button.pack_forget()

    def set_button(self, row):
        print "Set", row
        if self.active_field == 0:
            self.set_button0.config(state="normal")
        elif self.active_field == 1:
            self.set_button1.config(state="normal")
        elif self.active_field == 2:
            self.set_button2.config(state="normal")
        elif self.active_field == 3:
            self.set_button3.config(state="normal")
        elif self.active_field == 4:
            self.set_button4.config(state="normal")
        elif self.active_field == 5:
            self.set_button5.config(state="normal")
        elif self.active_field == 6:
            self.set_button6.config(state="normal")
        elif self.active_field == 7:
            self.set_button7.config(state="normal")
        elif self.active_field == 8:
            self.set_button8.config(state="normal")
        elif self.active_field == 9:
            self.set_button9.config(state="normal")
        elif self.active_field == 10:
            self.set_button_origin.config(state="normal")
        elif self.active_field == 11:
            self.set_button_center.config(state="normal")

        self.active_field = row

        if self.active_field == 0:
            self.set_button0.config(state="disabled")
        elif self.active_field == 1:
            self.set_button1.config(state="disabled")
        elif self.active_field == 2:
            self.set_button2.config(state="disabled")
        elif self.active_field == 3:
            self.set_button3.config(state="disabled")
        elif self.active_field == 4:
            self.set_button4.config(state="disabled")
        elif self.active_field == 5:
            self.set_button5.config(state="disabled")
        elif self.active_field == 6:
            self.set_button6.config(state="disabled")
        elif self.active_field == 7:
            self.set_button7.config(state="disabled")
        elif self.active_field == 8:
            self.set_button8.config(state="disabled")
        elif self.active_field == 9:
            self.set_button9.config(state="disabled")
        elif self.active_field == 10:
            self.set_button_origin.config(state="disabled")
        elif self.active_field == 11:
            self.set_button_center.config(state="disabled")

    def del_button(self, row):
        print "Del", row
        if row == 0:
            self.x_entry0.delete("1.0", END)
            self.y_entry0.delete("1.0", END)
        elif row == 1:
            self.x_entry1.delete("1.0", END)
            self.y_entry1.delete("1.0", END)
        elif row == 2:
            self.x_entry2.delete("1.0", END)
            self.y_entry2.delete("1.0", END)
        elif row == 3:
            self.x_entry3.delete("1.0", END)
            self.y_entry3.delete("1.0", END)
        elif row == 4:
            self.x_entry4.delete("1.0", END)
            self.y_entry4.delete("1.0", END)
        elif row == 5:
            self.x_entry5.delete("1.0", END)
            self.y_entry5.delete("1.0", END)
        elif row == 6:
            self.x_entry6.delete("1.0", END)
            self.y_entry6.delete("1.0", END)
        elif row == 7:
            self.x_entry7.delete("1.0", END)
            self.y_entry7.delete("1.0", END)
        elif row == 8:
            self.x_entry8.delete("1.0", END)
            self.y_entry8.delete("1.0", END)
        elif row == 9:
            self.x_entry9.delete("1.0", END)
            self.y_entry9.delete("1.0", END)
        elif row == 10:
            self.x_entry_origin.delete("1.0", END)
            self.y_entry_origin.delete("1.0", END)
        elif row == 11:
            self.x_entry_center.delete("1.0", END)
            self.y_entry_center.delete("1.0", END)

    def test_button(self, row):
        x = ''
        y = ''
        # self.w.move_pole((-3,-3))

        if row == 0:
            x = self.x_entry0.get("1.0", END)[:-1]
            y = self.y_entry0.get("1.0", END)[:-1]
        elif row == 1:
            x = self.x_entry1.get("1.0", END)[:-1]
            y = self.y_entry1.get("1.0", END)[:-1]
        elif row == 2:
            x = self.x_entry2.get("1.0", END)[:-1]
            y = self.y_entry2.get("1.0", END)[:-1]
        elif row == 3:
            x = self.x_entry3.get("1.0", END)[:-1]
            y = self.y_entry3.get("1.0", END)[:-1]
        elif row == 4:
            x = self.x_entry4.get("1.0", END)[:-1]
            y = self.y_entry4.get("1.0", END)[:-1]
        elif row == 5:
            x = self.x_entry5.get("1.0", END)[:-1]
            y = self.y_entry5.get("1.0", END)[:-1]
        elif row == 6:
            x = self.x_entry6.get("1.0", END)[:-1]
            y = self.y_entry6.get("1.0", END)[:-1]
        elif row == 7:
            x = self.x_entry7.get("1.0", END)[:-1]
            y = self.y_entry7.get("1.0", END)[:-1]
        elif row == 8:
            x = self.x_entry8.get("1.0", END)[:-1]
            y = self.y_entry8.get("1.0", END)[:-1]
        elif row == 9:
            x = self.x_entry9.get("1.0", END)[:-1]
            y = self.y_entry9.get("1.0", END)[:-1]

        if x != '' and y != '':
            offset = (int(x), int(y))
        else:
            offset = (0, 0)

        self.w.move_pole(self.coordinates[row], offset)
        self.w.trigger_pole()

    def done_button(self):
        if self.state == state_grid:
            print "Grid calibration done"

            if self.active_field == 0:
                self.set_button0.config(state="normal")
            elif self.active_field == 1:
                self.set_button1.config(state="normal")
            elif self.active_field == 2:
                self.set_button2.config(state="normal")
            elif self.active_field == 3:
                self.set_button3.config(state="normal")
            elif self.active_field == 4:
                self.set_button4.config(state="normal")
            elif self.active_field == 5:
                self.set_button5.config(state="normal")
            elif self.active_field == 6:
                self.set_button6.config(state="normal")
            elif self.active_field == 7:
                self.set_button7.config(state="normal")
            elif self.active_field == 8:
                self.set_button8.config(state="normal")
            elif self.active_field == 9:
                self.set_button9.config(state="normal")
            elif self.active_field == 10:
                self.set_button_origin.config(state="normal")
            elif self.active_field == 11:
                self.set_button_center.config(state="normal")
            self.active_field = -1

            x1 = self.x_entry1.get("1.0", END)[:-1]  # E
            x3 = self.x_entry3.get("1.0", END)[:-1]  # W

            y0 = self.y_entry0.get("1.0", END)[:-1]  # N
            y2 = self.y_entry2.get("1.0", END)[:-1]  # S

            try:
                if x1 == '' or x3 == '' or y0 == '' or y2 == '':
                    self.w.calibrate_grid_from_pond()
                else:
                    self.w.calibrate_grid_from_points(north=int(y0), east=int(x1), south=int(y2), west=int(x3))
            except:
                self.heading.config(text="Grid Calibration\nFAILED.")
                return

            self.forget_grid_calibration()
            self.pack_mouth_calibration()
            self.w.show_grid()
            self.set_image("gridlines.jpg")

            self.x_entry0.delete("1.0", END)
            self.y_entry0.delete("1.0", END)
            self.x_entry1.delete("1.0", END)
            self.y_entry1.delete("1.0", END)
            self.x_entry2.delete("1.0", END)
            self.y_entry2.delete("1.0", END)
            self.x_entry3.delete("1.0", END)
            self.y_entry3.delete("1.0", END)

            self.state = state_mouths

        elif self.state == state_mouths:
            print "Mouth calibration done"

            if self.active_field == 0:
                self.set_button0.config(state="normal")
            elif self.active_field == 1:
                self.set_button1.config(state="normal")
            elif self.active_field == 2:
                self.set_button2.config(state="normal")
            elif self.active_field == 3:
                self.set_button3.config(state="normal")
            elif self.active_field == 4:
                self.set_button4.config(state="normal")
            elif self.active_field == 5:
                self.set_button5.config(state="normal")
            elif self.active_field == 6:
                self.set_button6.config(state="normal")
            elif self.active_field == 7:
                self.set_button7.config(state="normal")
            elif self.active_field == 8:
                self.set_button8.config(state="normal")
            elif self.active_field == 9:
                self.set_button9.config(state="normal")
            elif self.active_field == 10:
                self.set_button_origin.config(state="normal")
            elif self.active_field == 11:
                self.set_button_center.config(state="normal")
            self.active_field = -1

            x0 = self.x_entry0.get("1.0", END)[:-1]
            x1 = self.x_entry1.get("1.0", END)[:-1]
            x2 = self.x_entry2.get("1.0", END)[:-1]
            x3 = self.x_entry3.get("1.0", END)[:-1]
            x4 = self.x_entry4.get("1.0", END)[:-1]
            x5 = self.x_entry5.get("1.0", END)[:-1]
            x6 = self.x_entry6.get("1.0", END)[:-1]
            x7 = self.x_entry7.get("1.0", END)[:-1]
            x8 = self.x_entry8.get("1.0", END)[:-1]
            x9 = self.x_entry9.get("1.0", END)[:-1]

            y0 = self.y_entry0.get("1.0", END)[:-1]
            y1 = self.y_entry1.get("1.0", END)[:-1]
            y2 = self.y_entry2.get("1.0", END)[:-1]
            y3 = self.y_entry3.get("1.0", END)[:-1]
            y4 = self.y_entry4.get("1.0", END)[:-1]
            y5 = self.y_entry5.get("1.0", END)[:-1]
            y6 = self.y_entry6.get("1.0", END)[:-1]
            y7 = self.y_entry7.get("1.0", END)[:-1]
            y8 = self.y_entry8.get("1.0", END)[:-1]
            y9 = self.y_entry9.get("1.0", END)[:-1]

            self.set_button0.pack_forget()
            self.del_button0.pack_forget()
            self.set_button1.pack_forget()
            self.del_button1.pack_forget()
            self.set_button2.pack_forget()
            self.del_button2.pack_forget()
            self.set_button3.pack_forget()
            self.del_button3.pack_forget()
            self.set_button4.pack_forget()
            self.del_button4.pack_forget()
            self.set_button5.pack_forget()
            self.del_button5.pack_forget()
            self.set_button6.pack_forget()
            self.del_button6.pack_forget()
            self.set_button7.pack_forget()
            self.del_button7.pack_forget()
            self.set_button8.pack_forget()
            self.del_button8.pack_forget()
            self.set_button9.pack_forget()
            self.del_button9.pack_forget()

            self.test_button0.pack(side=LEFT)
            self.test_button1.pack(side=LEFT)
            self.test_button2.pack(side=LEFT)
            self.test_button3.pack(side=LEFT)
            self.test_button4.pack(side=LEFT)
            self.test_button5.pack(side=LEFT)
            self.test_button6.pack(side=LEFT)
            self.test_button7.pack(side=LEFT)
            self.test_button8.pack(side=LEFT)
            self.test_button9.pack(side=LEFT)

            self.label0.config(text="Offset0")
            self.label1.config(text="Offset1")
            self.label2.config(text="Offset2")
            self.label3.config(text="Offset3")
            self.label4.config(text="Offset4")
            self.label5.config(text="Offset5")
            self.label6.config(text="Offset6")
            self.label7.config(text="Offset7")
            self.label8.config(text="Offset8")
            self.label9.config(text="Offset9")

            x_origin = self.x_entry_origin.get("1.0", END)[:-1]
            x_center = self.x_entry_center.get("1.0", END)[:-1]

            y_origin = self.y_entry_origin.get("1.0", END)[:-1]
            y_center = self.y_entry_center.get("1.0", END)[:-1]

            raw_coordinates = [(x0, y0),
                               (x1, y1),
                               (x2, y2),
                               (x3, y3),
                               (x4, y4),
                               (x5, y5),
                               (x6, y6),
                               (x7, y7),
                               (x8, y8),
                               (x9, y9)]

            self.coordinates = []
            for coord in raw_coordinates:
                if coord[0] != '' and coord[1] != '':
                    self.coordinates.append((int(coord[0]), int(coord[1])))

            if x_origin != '' and y_origin != '':
                self.origin = (int(x_origin), int(y_origin))
            else:
                self.origin = (0, 0)

            if x_center != '' and y_center != '':
                self.center = (int(x_center), int(y_center))
            else:
                self.center = (0, 0)

            self.w.origin = self.origin
            self.w.center = self.center

            print "Origin:", self.origin
            print "Center:", self.center

            """
            # Sort coordinates by proximity to origin
            distance_list = []
            for coord in self.coordinates:
                distance = math.sqrt(pow((coord[0] - self.origin[0]), 2) + pow((coord[1] - self.origin[1]), 2))
                distance_list.append(distance)
            sorted_distance_list = sorted(distance_list)

            sorted_coordinates = []
            for coord in sorted_distance_list:
                sorted_coordinates.append(self.coordinates[distance_list.index(coord)])
            self.coordinates = sorted_coordinates
            """

            self.w.mouths = self.coordinates
            print "Mouths:", self.coordinates

            self.x_entry0.delete("1.0", END)
            self.y_entry0.delete("1.0", END)
            self.x_entry1.delete("1.0", END)
            self.y_entry1.delete("1.0", END)
            self.x_entry2.delete("1.0", END)
            self.y_entry2.delete("1.0", END)
            self.x_entry3.delete("1.0", END)
            self.y_entry3.delete("1.0", END)
            self.x_entry4.delete("1.0", END)
            self.y_entry4.delete("1.0", END)
            self.x_entry5.delete("1.0", END)
            self.y_entry5.delete("1.0", END)
            self.x_entry6.delete("1.0", END)
            self.y_entry6.delete("1.0", END)
            self.x_entry7.delete("1.0", END)
            self.y_entry7.delete("1.0", END)
            self.x_entry8.delete("1.0", END)
            self.y_entry8.delete("1.0", END)
            self.x_entry9.delete("1.0", END)
            self.y_entry9.delete("1.0", END)

            if x0 == '' or y0 == '':
                self.row0.pack_forget()
                self.label0.pack_forget()
                self.x_entry0.pack_forget()
                self.y_entry0.pack_forget()
                self.test_button0.pack_forget()
            else:
                self.x_entry0.delete("1.0", END)
                self.x_entry0.insert(END, str(initial_x_offset))
                self.y_entry0.delete("1.0", END)
                self.y_entry0.insert(END, str(initial_y_offset))

            if x1 == '' or y1 == '':
                self.row1.pack_forget()
                self.label1.pack_forget()
                self.x_entry1.pack_forget()
                self.y_entry1.pack_forget()
                self.test_button1.pack_forget()
            else:
                self.x_entry1.delete("1.0", END)
                self.x_entry1.insert(END, str(initial_x_offset))
                self.y_entry1.delete("1.0", END)
                self.y_entry1.insert(END, str(initial_y_offset))

            if x2 == '' or y2 == '':
                self.row2.pack_forget()
                self.label2.pack_forget()
                self.x_entry2.pack_forget()
                self.y_entry2.pack_forget()
                self.test_button2.pack_forget()
            else:
                self.x_entry2.delete("1.0", END)
                self.x_entry2.insert(END, str(initial_x_offset))
                self.y_entry2.delete("1.0", END)
                self.y_entry2.insert(END, str(initial_y_offset))

            if x3 == '' or y3 == '':
                self.row3.pack_forget()
                self.label3.pack_forget()
                self.x_entry3.pack_forget()
                self.y_entry3.pack_forget()
                self.test_button3.pack_forget()
            else:
                self.x_entry3.delete("1.0", END)
                self.x_entry3.insert(END, str(initial_x_offset))
                self.y_entry3.delete("1.0", END)
                self.y_entry3.insert(END, str(initial_y_offset))

            if x4 == '' or y4 == '':
                self.row4.pack_forget()
                self.label4.pack_forget()
                self.x_entry4.pack_forget()
                self.y_entry4.pack_forget()
                self.test_button4.pack_forget()
            else:
                self.x_entry4.delete("1.0", END)
                self.x_entry4.insert(END, str(initial_x_offset))
                self.y_entry4.delete("1.0", END)
                self.y_entry4.insert(END, str(initial_y_offset))

            if x5 == '' or y5 == '':
                self.row5.pack_forget()
                self.label5.pack_forget()
                self.x_entry5.pack_forget()
                self.y_entry5.pack_forget()
                self.test_button5.pack_forget()
            else:
                self.x_entry5.delete("1.0", END)
                self.x_entry5.insert(END, str(initial_x_offset))
                self.y_entry5.delete("1.0", END)
                self.y_entry5.insert(END, str(initial_y_offset))

            if x6 == '' or y6 == '':
                self.row6.pack_forget()
                self.label6.pack_forget()
                self.x_entry6.pack_forget()
                self.y_entry6.pack_forget()
                self.test_button6.pack_forget()
            else:
                self.x_entry6.delete("1.0", END)
                self.x_entry6.insert(END, str(initial_x_offset))
                self.y_entry6.delete("1.0", END)
                self.y_entry6.insert(END, str(initial_y_offset))

            if x7 == '' or y7 == '':
                self.row7.pack_forget()
                self.label7.pack_forget()
                self.x_entry7.pack_forget()
                self.y_entry7.pack_forget()
                self.test_button7.pack_forget()
            else:
                self.x_entry7.delete("1.0", END)
                self.x_entry7.insert(END, str(initial_x_offset))
                self.y_entry7.delete("1.0", END)
                self.y_entry7.insert(END, str(initial_y_offset))

            if x8 == '' or y8 == '':
                self.row8.pack_forget()
                self.label8.pack_forget()
                self.x_entry8.pack_forget()
                self.y_entry8.pack_forget()
                self.test_button8.pack_forget()
            else:
                self.x_entry8.delete("1.0", END)
                self.x_entry8.insert(END, str(initial_x_offset))
                self.y_entry8.delete("1.0", END)
                self.y_entry8.insert(END, str(initial_y_offset))

            if x9 == '' or y9 == '':
                self.row9.pack_forget()
                self.label9.pack_forget()
                self.x_entry9.pack_forget()
                self.y_entry9.pack_forget()
                self.test_button9.pack_forget()
            else:
                self.x_entry9.delete("1.0", END)
                self.x_entry9.insert(END, str(initial_x_offset))
                self.y_entry9.delete("1.0", END)
                self.y_entry9.insert(END, str(initial_y_offset))

            self.row_origin.pack_forget()
            self.label_origin.pack_forget()
            self.x_entry_origin.pack_forget()
            self.y_entry_origin.pack_forget()
            self.set_button_origin.pack_forget()
            self.del_button_origin.pack_forget()

            self.row_center.pack_forget()
            self.label_center.pack_forget()
            self.x_entry_center.pack_forget()
            self.y_entry_center.pack_forget()
            self.set_button_center.pack_forget()
            self.del_button_center.pack_forget()
            self.buffer_row1.pack()

            self.state = state_offsets

        elif self.state == state_offsets:
            print "Offsets done"

            if self.active_field == 0:
                self.set_button0.config(state="normal")
            elif self.active_field == 1:
                self.set_button1.config(state="normal")
            elif self.active_field == 2:
                self.set_button2.config(state="normal")
            elif self.active_field == 3:
                self.set_button3.config(state="normal")
            elif self.active_field == 4:
                self.set_button4.config(state="normal")
            elif self.active_field == 5:
                self.set_button5.config(state="normal")
            elif self.active_field == 6:
                self.set_button6.config(state="normal")
            elif self.active_field == 7:
                self.set_button7.config(state="normal")
            elif self.active_field == 8:
                self.set_button8.config(state="normal")
            elif self.active_field == 9:
                self.set_button9.config(state="normal")
            elif self.active_field == 10:
                self.set_button_origin.config(state="normal")
            elif self.active_field == 11:
                self.set_button_center.config(state="normal")
            self.active_field = -1

            x0 = self.x_entry0.get("1.0", END)[:-1]
            x1 = self.x_entry1.get("1.0", END)[:-1]
            x2 = self.x_entry2.get("1.0", END)[:-1]
            x3 = self.x_entry3.get("1.0", END)[:-1]
            x4 = self.x_entry4.get("1.0", END)[:-1]
            x5 = self.x_entry5.get("1.0", END)[:-1]
            x6 = self.x_entry6.get("1.0", END)[:-1]
            x7 = self.x_entry7.get("1.0", END)[:-1]
            x8 = self.x_entry8.get("1.0", END)[:-1]
            x9 = self.x_entry9.get("1.0", END)[:-1]

            y0 = self.y_entry0.get("1.0", END)[:-1]
            y1 = self.y_entry1.get("1.0", END)[:-1]
            y2 = self.y_entry2.get("1.0", END)[:-1]
            y3 = self.y_entry3.get("1.0", END)[:-1]
            y4 = self.y_entry4.get("1.0", END)[:-1]
            y5 = self.y_entry5.get("1.0", END)[:-1]
            y6 = self.y_entry6.get("1.0", END)[:-1]
            y7 = self.y_entry7.get("1.0", END)[:-1]
            y8 = self.y_entry8.get("1.0", END)[:-1]
            y9 = self.y_entry9.get("1.0", END)[:-1]

            raw_offsets = [(x0, y0),
                           (x1, y1),
                           (x2, y2),
                           (x3, y3),
                           (x4, y4),
                           (x5, y5),
                           (x6, y6),
                           (x7, y7),
                           (x8, y8),
                           (x9, y9)]

            self.offsets = []
            for offset in raw_offsets:
                if offset[0] != '' and offset[1] != '':
                    self.offsets.append((int(offset[0]), int(offset[1])))

            print "Offsets", self.offsets

            self.forget_mouth_calibration()

            self.input.pack()
            self.button1.pack()
            self.button2.pack()
            self.buffer_row2.pack()
            self.reset_button.pack()

            self.heading.config(text="Find Red Fish")
            self.button1.config(text="Find Red", state=ACTIVE)
            self.button2.config(state=ACTIVE)
            self.input.delete("1.0", END)
            self.input.insert(END, str(self.red_threshold))
            self.state = state_red
            self.set_image("pond.jpg")

    def set_coordinates(self, event):
        print "Loading", (event.x, event.y)
        if self.active_field == 0:
            self.x_entry0.delete("1.0", END)
            self.x_entry0.insert(END, event.x)
            self.y_entry0.delete("1.0", END)
            self.y_entry0.insert(END, event.y)
        elif self.active_field == 1:
            self.x_entry1.delete("1.0", END)
            self.x_entry1.insert(END, event.x)
            self.y_entry1.delete("1.0", END)
            self.y_entry1.insert(END, event.y)
        elif self.active_field == 2:
            self.x_entry2.delete("1.0", END)
            self.x_entry2.insert(END, event.x)
            self.y_entry2.delete("1.0", END)
            self.y_entry2.insert(END, event.y)
        elif self.active_field == 3:
            self.x_entry3.delete("1.0", END)
            self.x_entry3.insert(END, event.x)
            self.y_entry3.delete("1.0", END)
            self.y_entry3.insert(END, event.y)
        elif self.active_field == 4:
            self.x_entry4.delete("1.0", END)
            self.x_entry4.insert(END, event.x)
            self.y_entry4.delete("1.0", END)
            self.y_entry4.insert(END, event.y)
        elif self.active_field == 5:
            self.x_entry5.delete("1.0", END)
            self.x_entry5.insert(END, event.x)
            self.y_entry5.delete("1.0", END)
            self.y_entry5.insert(END, event.y)
        elif self.active_field == 6:
            self.x_entry6.delete("1.0", END)
            self.x_entry6.insert(END, event.x)
            self.y_entry6.delete("1.0", END)
            self.y_entry6.insert(END, event.y)
        elif self.active_field == 7:
            self.x_entry7.delete("1.0", END)
            self.x_entry7.insert(END, event.x)
            self.y_entry7.delete("1.0", END)
            self.y_entry7.insert(END, event.y)
        elif self.active_field == 8:
            self.x_entry8.delete("1.0", END)
            self.x_entry8.insert(END, event.x)
            self.y_entry8.delete("1.0", END)
            self.y_entry8.insert(END, event.y)
        elif self.active_field == 9:
            self.x_entry9.delete("1.0", END)
            self.x_entry9.insert(END, event.x)
            self.y_entry9.delete("1.0", END)
            self.y_entry9.insert(END, event.y)
        elif self.active_field == 10:
            self.x_entry_origin.delete("1.0", END)
            self.x_entry_origin.insert(END, event.x)
            self.y_entry_origin.delete("1.0", END)
            self.y_entry_origin.insert(END, event.y)
        elif self.active_field == 11:
            self.x_entry_center.delete("1.0", END)
            self.x_entry_center.insert(END, event.x)
            self.y_entry_center.delete("1.0", END)
            self.y_entry_center.insert(END, event.y)


if __name__ == '__main__':
    root = Tk()
    app = App(root)
    root.mainloop()
