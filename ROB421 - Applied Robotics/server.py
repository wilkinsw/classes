#!/usr/bin/python
from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
from os import curdir, sep
import cgi

PORT_NUMBER = 80
current_point = 0
origin_set = True
dict = {'0':('0','0'),
        '1':('0', '0'),
        '2':('0', '0'),
        '3':('0', '0'),
        '4':('0', '0'),
        '5':('0', '0'),
        '6':('0', '0'),
        '7':('0', '0'),
        '8':('0', '0'),
        '9':('0', '0'),
        '10':('0','0'),
        '11':('0', '0'),
        '12':('0', '0'),
        '13':('0', '0'),
        '14':('0', '0'),
        '15':('0', '0'),
        '16':('0', '0'),
        '17':('0', '0'),
        '18':('0', '0'),
        '19':('0', '0')}

#This class will handles any incoming request from
#the browser 
class myHandler(BaseHTTPRequestHandler):

    def print_wilson_index(self, point=1, set_origin=True):
        self.wfile.write("""
        <html>
            <head>
                <title>Wilson</title>
                <link rel="stylesheet" href="main.css">
                <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>""")
        if set_origin:
            self.wfile.write("""
                <script>
                    $(function() {
                        $("#picture").click(function(e) {
                            var img = document.getElementById('pic');
                            var height = img.clientHeight;
                            var offset = $(this).offset();
                            var relativeX = (e.pageX - offset.left);
                            var relativeY = (e.pageY - offset.top - 13) + height;
                            $("#x0").val (relativeX);
                            $("#y0").val (relativeY);
                        });
                    });
                </script>""")
        else:
            self.wfile.write("""
                <script>
                    $(function() {
                        $("#picture").click(function(e) {
                            var img = document.getElementById('pic');
                            var height = img.clientHeight;
                            var offset = $(this).offset();
                            var relativeX = (e.pageX - offset.left);
                            var relativeY = (e.pageY - offset.top - 13) + height;""")
            self.wfile.write("$(\"#x" + str(point) + "\").val (relativeX);\n")
            self.wfile.write("$(\"#y" + str(point) + "\").val (relativeY);\n")
            self.wfile.write("""
                        });
                    });
                </script>""")

        self.wfile.write("""
            </head>
            <body>
                <div id='shadow-one'><div id='shadow-two'><div id='shadow-three'><div id='shadow-four'>
                    <div id='page'>
                        <div id='title'><div class='right'>Calibration</div><span id='hello'>&nbsp;</span></div>
                        <div id='menu'>
                            <table>
                                <tr>
                                    <td colspan="4">
                                        <h2>Arm Offset</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <form method="GET">
                                        <td>
                                            <label>(0,0)</label>
                                        </td>""")
        if set_origin:
            self.wfile.write("<td>")
            self.wfile.write("<input type='text' id='x0' size='2' name='x0' value='" + dict['0'][0] + "'/>")
            self.wfile.write("</td>")
            self.wfile.write("<td>")
            self.wfile.write("<input type='text' id='y0' size='2' name='y0'' value='" + dict['0'][1] + "'/>")
            self.wfile.write("</td>")
            self.wfile.write("<td>")
            self.wfile.write("<button type='submit' name='set' value='0' >Set</button>")
            self.wfile.write("</td>")
            self.wfile.write("""
                                    </form>
                                </tr>
                                <tr><td>&nbsp;</td></tr>
                                <tr>
                                    <td colspan="3">
                                        <h2>Open Mouths</h2>
                                    </td>
                                </tr>""")

            iter = 1
            while iter < point:
                self.wfile.write("<form method='GET'>\n")
                self.wfile.write("<tr>\n")
                self.wfile.write("<td>\n")
                self.wfile.write("<label>Pt" + str(iter) + "</label>\n")
                self.wfile.write("</td>\n")
                self.wfile.write("<td>\n")
                self.wfile.write("<input type='text' size='2' name='x" + str(iter) + "' id='x" + str(iter) + "' value='" + dict[str(iter)][0] + "' disabled/>\n")
                self.wfile.write("</td>\n")
                self.wfile.write("<td>\n")
                self.wfile.write("<input type='text' size='2' name='y" + str(iter) + "' id='y" + str(iter) + "' value='" + dict[str(iter)][1] + "' disabled/>\n")
                self.wfile.write("</td>\n")
                self.wfile.write("<td>\n")
                self.wfile.write("<button type='submit' name='del' value='" + str(iter) + "' >Del</button>\n")
                self.wfile.write("</td>\n")
                self.wfile.write("</tr>\n")
                self.wfile.write("</form>\n")
                iter = iter + 1

            self.wfile.write("<form method='GET'>\n")
            self.wfile.write("<tr>\n")
            self.wfile.write("<td>\n")
            self.wfile.write("<label>Pt" + str(point) + "</label>\n")
            self.wfile.write("</td>\n")
            self.wfile.write("<td>\n")
            self.wfile.write("<input type='text' size='2' name='x" + str(point) + "' id='x0" + str(point) + "' value='" + dict[str(point)][0] + "' disabled/>\n")
            self.wfile.write("</td>\n")
            self.wfile.write("<td>\n")
            self.wfile.write("<input type='text' size='2' name='y" + str(point) + "' id='y0" + str(point) + "' value='" + dict[str(point)][1] + "' disabled/>\n")
            self.wfile.write("</td>\n")
            self.wfile.write("<td>\n")
            self.wfile.write("<button type='submit' name='set' value='" + str(point) + "' disabled>Set</button>\n")
            self.wfile.write("</td>\n")
            self.wfile.write("</tr>\n")
            self.wfile.write("</form>\n")

        else:
            self.wfile.write("<td>")
            self.wfile.write("<input type='text' id='x0' size='2' name='x0' value='" + dict['0'][0] + "' disabled/>")
            self.wfile.write("</td>")
            self.wfile.write("<td>")
            self.wfile.write("<input type='text' id='y0' size='2' name='y0'' value='" + dict['0'][1] + "' disabled/>")
            self.wfile.write("</td>")
            self.wfile.write("<td>")
            self.wfile.write("<button type='submit' name='del' value='0' >Del</button>")
            self.wfile.write("</td>")
            self.wfile.write("""
                                    </form>
                                </tr>
                                <tr><td>&nbsp;</td></tr>
                                <tr>
                                    <td colspan="3">
                                        <h2>Open Mouths</h2>
                                    </td>
                                </tr>""")

            iter = 1
            while iter < point:
                self.wfile.write("<form method='GET'>\n")
                self.wfile.write("<tr>\n")
                self.wfile.write("<td>\n")
                self.wfile.write("<label>Pt" + str(iter) + "</label>\n")
                self.wfile.write("</td>\n")
                self.wfile.write("<td>\n")
                self.wfile.write("<input type='text' size='2' name='x" + str(iter) + "' id='x" + str(iter) + "' value='" + dict[str(iter)][0] + "' disabled/>\n")
                self.wfile.write("</td>\n")
                self.wfile.write("<td>\n")
                self.wfile.write("<input type='text' size='2' name='y" + str(iter) + "' id='y" + str(iter) + "' value='" + dict[str(iter)][1] + "' disabled/>\n")
                self.wfile.write("</td>\n")
                self.wfile.write("<td>\n")
                self.wfile.write("<button type='submit' name='del' value='" + str(iter) + "' >Del</button>\n")
                self.wfile.write("</td>\n")
                self.wfile.write("</tr>\n")
                self.wfile.write("</form>\n")
                iter = iter + 1

            self.wfile.write("<form method='GET'>\n")
            self.wfile.write("<tr>\n")
            self.wfile.write("<td>\n")
            self.wfile.write("<label>Pt" + str(point) + "</label>\n")
            self.wfile.write("</td>\n")
            self.wfile.write("<td>\n")
            self.wfile.write("<input type='text' size='2' name='x" + str(point) + "' id='x" + str(point) + "' value='" + dict[str(point)][0] + "' '/>\n")
            self.wfile.write("</td>\n")
            self.wfile.write("<td>\n")
            self.wfile.write("<input type='text' size='2' name='y" + str(point) + "' id='y" + str(point) + "' value='" + dict[str(point)][1] + "' '/>\n")
            self.wfile.write("</td>\n")
            self.wfile.write("<td>\n")
            self.wfile.write("<button type='submit' name='set' value='" + str(point) + "' >Set</button>\n")
            self.wfile.write("</td>\n")
            self.wfile.write("</tr>\n")
            self.wfile.write("</form>\n")

        self.wfile.write("""
                    </table>
                </div>

                <div id='content'>
                    <fieldset>
                    <a id="picture">
                        <img id="pic" src="out/marked_fish.jpg" />
                    </a>
                    </fieldset>
                </div>

                <div class='spacer'>&nbsp;</div>

                <div id='footer'>
                    <p>Team 16: Cast Away</p>
                </div>
            </div>
        </div></div></div></div>
    </body>
    </html>""")

    #Handler for the GET requests
    def do_GET(self):
        global origin_set
        global current_point
        global dict
        get_string = ""
        set = ""
        x_value = ""
        y_value = ""

        if self.path=="/":
            self.send_response(200)
            self.send_header('Content-type','text/html')
            self.end_headers()
            self.print_wilson_index(current_point, origin_set)
        elif self.path.startswith("/?"):
            self.send_response(200)
            self.send_header('Content-type','text/html')
            self.end_headers()

            get_string = self.path.split("?")[1]
            var_list = get_string.split("&")
            for var in var_list:
                command = var.split("=")[0]
                value = var.split("=")[1]

                if command == "del":
                    dict[value] = ('0', '0')
                    if value == '0':
                        current_point = 1
                        origin_set = True
                    else:
                        current_point = int(value)
                    self.print_wilson_index(current_point, origin_set)
                    return
                elif command == "set":
                    set = value
                elif command[0] == 'x':
                    x_value = value
                elif command[0] == 'y':
                    y_value = value
                else:
                    print "Bad command"

            dict[set] = (x_value, y_value)
            print set, "set to (", x_value, y_value, ")"
            while dict[str(current_point)] != ('0', '0'):
                current_point = current_point + 1
            origin_set = False
            self.print_wilson_index(current_point, origin_set)

        else:
            try:
                #Check the file extension required and
                #set the right mime type

                sendReply = False
                if self.path.endswith(".html"):
                    mimetype='text/html'
                    sendReply = True
                if self.path.endswith(".jpg"):
                    mimetype='image/jpg'
                    sendReply = True
                if self.path.endswith(".gif"):
                    mimetype='image/gif'
                    sendReply = True
                if self.path.endswith(".js"):
                    mimetype='application/javascript'
                    sendReply = True
                if self.path.endswith(".css"):
                    mimetype='text/css'
                    sendReply = True

                if sendReply == True:
                    #Open the static file requested and send it
                    f = open(curdir + sep + self.path)
                    self.send_response(200)
                    self.send_header('Content-type',mimetype)
                    self.end_headers()
                    self.wfile.write(f.read())
                    f.close()
                return

            except IOError:
                self.send_error(404,'File Not Found: %s' % self.path)

try:
    #Create a web server and define the handler to manage the
    #incoming request
    server = HTTPServer(('', PORT_NUMBER), myHandler)
    print 'Started httpserver on port ' , PORT_NUMBER

    #Wait forever for incoming htto requests
    server.serve_forever()

except KeyboardInterrupt:
    print '^C received, shutting down the web server'
    server.socket.close()

