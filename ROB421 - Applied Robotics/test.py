#import wilson
#from SimpleCV import Color

from wilson import xy_to_polar, polar_to_xy

"""
w = wilson.Wilson()

# Calibration
w.load_image("debug.png")
w.find_pond()
w.set_skew()
w.calibrate_grid_from_pond(100)
w.find_red_fish()
w.find_yellow_fish()
w.find_green_fish()
w.mark_fish(filename="out/one.jpg")

# Run
w.load_image("debug.png")
w.find_pond(clean=False)
w.find_red_fish(clean=False)
w.pond_img.colorDistance(Color.RED).binarize(100).save("out/red.jpg")
w.find_yellow_fish(clean=False)
w.pond_img.colorDistance(Color.YELLOW).binarize(80).save("out/yellow.jpg")
w.find_green_fish(clean=False)
w.pond_img.colorDistance(Color.GREEN).binarize(50).save("out/green.jpg")
w.mark_fish(filename="out/result.jpg")
"""

# Mouths: [(277, 440), (487, 232), (486, 635), (696, 420)]
# Offset: [(610, 610), (318, 564), (643, 327), (346, 284)]
"""
center = (487, 441)

point1 = (277, 440)
point2 = (610, 610)

x = point1[0] - center[0]
y = point1[1] - center[1]

print "X",x,"Y",y
r, theta = xy_to_polar(x, y)

print "radius",r
print "angle", theta, "degrees", theta/3.14159*180

new_angle = theta - 2
x2, y2 = polar_to_xy(r, new_angle)

print "X2", x2+center[0], "Y2", y2+center[1]
"""

r, theta = xy_to_polar(1, 0)
print "angle", theta, "degrees", theta/3.14159*180
print polar_to_xy(r, theta)
r, theta = xy_to_polar(1, 1)
print "angle", theta, "degrees", theta/3.14159*180
print polar_to_xy(r, theta)
r, theta = xy_to_polar(0, 1)
print "angle", theta, "degrees", theta/3.14159*180
print polar_to_xy(r, theta)
r, theta = xy_to_polar(-1, 1)
print "angle", theta, "degrees", theta/3.14159*180
print polar_to_xy(r, theta)
r, theta = xy_to_polar(-1, 0)
print "angle", theta, "degrees", theta/3.14159*180
print polar_to_xy(r, theta)
r, theta = xy_to_polar(-1, -1)
print "angle", theta, "degrees", theta/3.14159*180
print polar_to_xy(r, theta)
r, theta = xy_to_polar(0, -1)
print "angle", theta, "degrees", theta/3.14159*180
print polar_to_xy(r, theta)
r, theta = xy_to_polar(1, -1)
print "angle", theta, "degrees", theta/3.14159*180
print polar_to_xy(r, theta)