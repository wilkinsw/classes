#include <AFMotor.h>
#include <Servo.h> 

/****VARIABLES****/
float r,theta;
float X = 0; float Y = 1;
unsigned long time;
int first_run = 0;
int running_flag = 0;
int origin =78;
int start_angle = 60; // Angle to start rod at
int end_angle = 40;
float base=40.0;  //from the origin to the beginning of the rod and from the edge of the servo to its axis
float rod=200.0; //Length of the ro
float convert=31.746;
float r1,r2,theta1,theta2;
float xPrev=0;
int buttonPin=53;
int jon_pin = 30;

/****SERVO****/ 
Servo myservo;  // create servo object to control a servo 
int pos = 0;

/****STEPPER****/
AF_Stepper motor(100, 2);
AF_Stepper motorLin(200, 1);
int StepsTo180deg = 3200/2;
float theta0 = 90;

/****LINEAR ACTUATOR****/
int linPin=44;      // LED connected to digital pin 9
float valLin = 0;         // variable to store the read value1


void setup() {
  Serial.begin(115200);           // set up Serial library at 9600 bps
  //Serial1.begin(115200);
  pinMode(jon_pin, INPUT);
  Serial.println("Send the coordinate,    \"-9, -9\"  To lower the rod");
  Serial.println("                        \"-1, -1\"  Move back 10 mm");
  Serial.println("                        \"-2, -2\"  Move back 20 mm");
  Serial.println("                        \"-3, -3\"  Move back 30 mm");
  Serial.println("                        \"-4, -4\"  Move forward 10 mm");
  Serial.println("                        \"-5, -5\"  Move forward 20 mm");
  Serial.println("                        \"-6, -6\"  Move forward 30 mm");
  Serial.println("                        \"-7, -7\"  Move back 5 degrees CW");
  Serial.println("                        \"-8, -8\"  Move back 5 degrees CCW");
  Serial.println("                        \"-11, -11\" To release the fish");
  
  pinMode(buttonPin, INPUT_PULLUP);  
  
  motor.setSpeed(225);  // 10 rpm 
  motorLin.setSpeed(200);  // 10 rpm   

  myservo.attach(44);  // attaches the servo on pin 9 to the servo object 
  myservo.write(start_angle);
  
  //motorLin.step(, FORWARD, SINGLE);   
  
}

void loop() {
  button();
  while (Serial.available() > 0) {
    getCoordinates();
    if (X == -9 && Y == -9){ servoRod();}
    else if(X == -1,Y == -1){moveBack(10);}
    else if(X == -2,Y == -2){moveBack(20);}
    else if(X == -3,Y == -3){moveBack(30);}
    else if(X == -4,Y == -4){moveForward(10);}
    else if(X == -5,Y == -5){moveForward(20);}
    else if(X == -6,Y == -6){moveForward(30);}
    else if(X == -7,Y == -7){moveCW();}
    else if(X == -8,Y == -8){moveCCW();}
    else if(X == -11,Y == -11){releaseFish();}
    else {
      calcThetaAndR();
      stepper();
      linRail();       
    }
    Serial.println("done");
    if (r < 0){r=0;}//This negates negitive r values
  }
  
//  stopButton();
  
  
  
  
  
}








