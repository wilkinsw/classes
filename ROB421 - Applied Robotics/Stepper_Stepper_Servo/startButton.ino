void button()
{
  int sensorVal = digitalRead(buttonPin);
  
  if (running_flag && first_run)
  {
    if(sensorVal == HIGH)
    {
      Serial.println("stop");
      while(sensorVal == HIGH)
      {
        sensorVal = digitalRead(buttonPin);
        delay(100);
      }
      while (Serial.available() > 0) {Serial.read();} 
      running_flag = 0;
    }
  }
  else
  {
    if(sensorVal == LOW)
    {
      delay(100);
      int sensorVal = digitalRead(buttonPin);
      if(sensorVal == LOW)
      {
        first_run = 1;
        running_flag = 1;
        while (Serial.available() > 0) {Serial.read();} 
        Serial.println("play");
      }
    }
  }
}
