void calcThetaAndR(){
  r1=-base-rod-sqrt(X*X+Y*Y); 
  theta1=atan2(-(Y),-(X));
  r2= -base - rod + sqrt(X*X+Y*Y);
  theta2=atan2(Y,X);
  Serial.print("R1 ");
  Serial.print(r1);
  Serial.print("    R2 ");
  Serial.println(r2);

  if (r1 > 0  && r2 < 0){
    r = r1;
    theta = theta1*180/PI;  
  }
  else if (r1 < 0  && r2 > 0){
    r = r2;
    theta = theta2*180/PI;    
  }
  else if (r1 < 0 && r2 < 0){
    r = 10;
    theta = 90;
  }
  else{
    r = 10;
    theta = 90;
  }
  if (r > 60){r = 60;} 
  delay(10);
  Serial.print("Radius is ");
  Serial.println(r);
  Serial.println("");
  Serial.print("Theta is ");
  Serial.println(theta);

}
