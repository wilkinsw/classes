#include <time.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	int i, n;
	int number = 0;
	time_t t;
	
	if (argc < 2) {
        fprintf(stderr,"Usage: ./keygen keyLength\n");
        exit(1);
    }
	 
	srand((unsigned) time(&t));
	n = atoi(argv[1]);
	
   for( i = 0 ; i < n ; i++ ) 
   {
	   number = (rand() % 27)+65;
	   if(number == 91) number = 32; //If the 27th character make it a space
	   printf("%c", number);
   }
   printf("\n", number);
   
   return(0);
}