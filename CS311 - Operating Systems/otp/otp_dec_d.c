/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

#define debug 0

char otp_str[256];

// Does the OTP decryption and loads it into otp_str buffer.
void decrypt(char *key, char *ciphertext)
{
	int length = 0;
	int iter = 0;
	int ch = 0;
	bzero(otp_str,256);
	
	length = strlen(ciphertext);
	
	for(iter = 0; iter < length; iter++)
	{
		if(ciphertext[iter] == 32) ciphertext[iter] = 91;
		if(key[iter] == 32) key[iter] = 91;
		
		ch = ciphertext[iter] - key[iter];
		if(debug) printf("%d = %d - %d -> ", ch, ciphertext[iter], key[iter]);
		ch = (ch+27) % 27;
		if(debug) printf("%27 = %d -> ", ch);
		ch = ch + 65;
		if(ch == 91) ch = 32;
		if(debug) printf("%d (%c)\n", ch, ch);
		otp_str[iter] = ch;
	}
	otp_str[255] = '\0';
}

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[])
{
    int sockfd, newsockfd, portno;
    socklen_t clilen;
    char buffer[256], buffer2[256];
    struct sockaddr_in serv_addr, cli_addr;
    int n, i;
	pid_t pid;
	pid_t children[5];
    if (argc < 2) {
        fprintf(stderr,"ERROR, no port provided\n");
        exit(1);
    }
	
	// Spawns 5 child processes
	for (i=0; i<5; i++)
    {
        pid=fork();
        switch(pid)
        {
            case 0:
            {
                //child
				// Create socket for the child
				sockfd = socket(AF_INET, SOCK_STREAM, 0);
				if (sockfd < 0) error("ERROR opening socket");
				bzero((char *) &serv_addr, sizeof(serv_addr));
				portno = atoi(argv[1])+i+1;
				if (debug) printf("Child %d: Creating socket on port %d\n", i, portno);
				serv_addr.sin_family = AF_INET;
				serv_addr.sin_addr.s_addr = INADDR_ANY;
				serv_addr.sin_port = htons(portno);
				if (bind(sockfd, (struct sockaddr *) &serv_addr,
						 sizeof(serv_addr)) < 0) 
						 error("ERROR on binding");
				listen(sockfd,5);
				clilen = sizeof(cli_addr);
				
				// Read in the key and ciphertext then do the decryption and send it back
				while(1)
				{
					newsockfd = accept(sockfd, 
							(struct sockaddr *) &cli_addr, 
							&clilen);
					if (newsockfd < 0) error("ERROR on accept");
					 
					bzero(buffer,256);
					if(debug) printf("Child %d:  Waiting for handshake\n", i);
					n = read(newsockfd,buffer,255);
					if (n < 0)
					{
						if(debug) printf("Child %d:  ERROR reading from socket\n", i);
						close(newsockfd);
						continue;
					}
					if(debug) printf("Child %d:  Got: %s\n", i, buffer);
					if(strcmp(buffer, "otp_dec2") == 0)
					{
						bzero(buffer,256);
						sprintf(buffer, "connect");
						if(debug) printf("Child %d:  Accepting connection\n", i);
						n = write(newsockfd,buffer,255);
						if (n < 0)
						{
							if(debug) printf("Child %d:  ERROR writing to socket\n", i);
							close(newsockfd);
							continue;
						}
					}
					else
					{
						bzero(buffer,256);
						sprintf(buffer, "no");
						if(debug) printf("Child %d:  Rejecting connection\n", i);
						n = write(newsockfd,buffer,255);
						if (n < 0) printf("Child %d:  ERROR writing to socket\n", i);
						close(newsockfd);
						continue;
					}
					  
					while(1)
					{
						bzero(buffer,256);
						bzero(buffer2,256);
						n = read(newsockfd,buffer,255);
						if (n < 0) printf("Child %d:  ERROR reading from socket\n", i);
						if (n == 0) break;
						if(debug) printf("Child %d: %s\n",i,buffer);
						
						bzero(buffer2,256);
						n = read(newsockfd,buffer2,255);
						if (n < 0) printf("Child %d:  ERROR reading from socket\n", i);
						if (n == 0) break;
						if(debug) printf("Child %d: %s\n",i,buffer2);
						decrypt(buffer, buffer2);
						if(debug) printf("Child %d:  decryption is: %s\n", i, otp_str);
						n = write(newsockfd,otp_str,255);
						if (n < 0) printf("Child %d:  ERROR writing to socket\n", i);

						
					}
					close(newsockfd);
				}
				
                break;
            }
            default:
            {
                //parent
				children[i] = pid;
                break;
            }
        }
    }
	
	// Only the parent will run this code.
	// Create the socket and listen for connections.
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) error("ERROR opening socket");
    bzero((char *) &serv_addr, sizeof(serv_addr));
    portno = atoi(argv[1]);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);
    if (bind(sockfd, (struct sockaddr *) &serv_addr,
             sizeof(serv_addr)) < 0) 
             error("ERROR on binding");
    listen(sockfd,5);
    clilen = sizeof(cli_addr);
	
	// Wait for connections, check the handshake, and send it to a child.
	i = 0;
	while(1)
	{
		if(i>=5) i = 0;
		newsockfd = accept(sockfd, 
                (struct sockaddr *) &cli_addr, 
                &clilen);
		if (newsockfd < 0) error("ERROR on accept");
		 
		bzero(buffer,256);
		bzero(buffer,256);
		if(debug) printf("otp_dec_d:  Waiting for handshake\n");
		n = read(newsockfd,buffer,255);
		if (n < 0)
		{
			if(debug) printf("otp_dec_d:  ERROR reading from socket\n");
			close(newsockfd);
			continue;
		}
		if(debug) printf("otp_dec_d:  Got: %s\n", buffer);
		if(strcmp(buffer, "otp_dec") == 0)
		{
			bzero(buffer,256);
			sprintf(buffer, "connect %d end", portno+i+1);
			if(debug) printf("otp_dec_d:  Redirecting connection to port %d\n", portno+i+1);
			n = write(newsockfd,buffer,255);
			if (n < 0)
			{
				if(debug) printf("otp_dec_d:  ERROR writing to socket\n");
				close(newsockfd);
				continue;
			}
		}
		else
		{
			bzero(buffer,256);
			sprintf(buffer, "no");
			if(debug) printf("otp_dec_d:  Rejecting connection\n");
			n = write(newsockfd,buffer,255);
			if (n < 0)
			{
				if(debug) printf("otp_dec_d:  ERROR writing to socket\n");
			}
			close(newsockfd);
			continue;
		}
		  
		close(newsockfd);
		i++;
	}
     
     
     
    close(sockfd);
    return 0; 
}