#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

#define debug 0

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[])
{
    int sockfd, portno, n, i, iter, newport, len_encrypted, len_key, flag;
    struct sockaddr_in serv_addr;
    struct hostent *server;
	FILE *encrypted, *key; 
	char ch, ch2;
    char buffer[256], buffer2[256], result[256];
	char str1[50], str2[256];
	
    if (argc < 3) {
       fprintf(stderr,"otp_dec: usage %s encrypted key port\n", argv[0]);
       exit(0);
    }
	
	// Open the input files
	encrypted = fopen(argv[1], "r");
	key = fopen(argv[2], "r");
	
	// Iterate through the encrypted file and check that each character
	// is in the valid alphabet.  Also count the characters.
	ch = getc(encrypted);
	iter = 0;
    while(ch != EOF)
	{
		flag = 0;
		if(ch == 10) break;
		for( i = 0 ; i < 27 ; i++ ) 
	    {
		    if((ch == i+65) || (ch == 32)) flag = 1;
	    }
		if(!flag)
		{
			fprintf(stderr,"otp_dec: ERROR, invalid character found in encrypted\n");
			exit(1);
		}
		iter++;
		ch = getc(encrypted);
	}
	len_encrypted = iter;
	rewind(encrypted);
	iter = 0;
	
	// Iterate through the key file and check that each character
	// is in the valid alphabet.  Also count the characters.
	ch = getc(key);
	while(ch != EOF)
	{
		flag = 0;
		if(ch == 10) break;
		for( i = 0 ; i < 27 ; i++ ) 
	    {
		    if((ch == i+65) || (ch == 32)) flag = 1;
	    }
		if(!flag)
		{
			fprintf(stderr,"otp_dec: ERROR, invalid character found in key\n");
			exit(1);
		}
		iter++;
		ch = getc(key);
	}
	len_key = iter;
	rewind(key);
	iter = 0;
	
	// Check that the lengths are compatible.
	// Keys must be greater than or equal to encrypted length.
	if(len_encrypted > len_key)
	{
		fprintf(stderr,"otp_dec: ERROR, key can't be shorter than encrypted %d > %d\n", len_encrypted, len_key);
		exit(1);
	}
	
	
	// Sockets
    portno = atoi(argv[3]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("otp_dec: ERROR opening socket\n");
    server = gethostbyname("localhost");
    if (server == NULL) {
        fprintf(stderr,"otp_dec: ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        error("otp_dec: ERROR connecting");
	
	// Do handshake with first process.
	// Will recieve back the port of the child to reconnect to, else error out.
	if(debug) printf("otp_dec: First handshake\n");
	bzero(buffer,256);
	sprintf(buffer, "otp_dec");	
	if(debug) printf("otp_dec: Writing %s\n", buffer);
	n = write(sockfd,buffer,255);
	if (n < 0) error("otp_dec: ERROR writing handshake\n");
	bzero(buffer,256);
	n = read(sockfd,buffer,255);
	if (n < 0) error("otp_dec: ERROR reading handshake\n");
	if(debug) printf("otp_dec: Read %s\n", buffer);
	sscanf(buffer, "%s %d %s", str1, &newport, str2);  // Scan out the port number
	if(strcmp(str1, "connect") != 0) // If handshake was wrong error out
	{
		fprintf(stderr,"otp_dec: ERROR, bad port %d (Can't use otp_enc_d)\n", portno);
        exit(2);
	}
	bzero(buffer,256);
	
	// Close the original socket and connect to child on new port
	if(debug) printf("New port is: %d", newport);
	close(sockfd);
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("otp_dec: ERROR opening socket\n");
    server = gethostbyname("localhost");
    if (server == NULL) {
        fprintf(stderr,"otp_dec: ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(newport);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        error("otp_dec: ERROR connecting");
	
	// Do new handshake with child process to make sure we're talking to the child.
	// The handshake is slightly different to be safe.
	bzero(buffer,256);
	sprintf(buffer, "otp_dec2");	
	n = write(sockfd,buffer,255);
	if (n < 0) error("otp_dec: ERROR writing handshake\n");
	bzero(buffer,256);
	n = read(sockfd,buffer,255);
	if (n < 0) error("otp_dec: ERROR reading handshake\n");
	if(strcmp(buffer, "connect") != 0) error("otp_dec: ERROR wrong server\n");
	bzero(buffer,256);
	bzero(buffer2,256);
	iter = 0;
	
	// Pull out a character at a time from each file and fill up the buffers.
	// Once the max buffer size is reached send the data and get/print the encrypted response.
	// This does the encryption in 255 bit chunks.
	ch = getc(key);
	ch2 = getc(encrypted);
    while(ch2 != EOF)
	{
		if(iter > 255)
		{
			n = write(sockfd,buffer,255);
			if (n < 0) error("otp_dec: ERROR writing1 to socket\n");
			n = write(sockfd,buffer2,255);
			if (n < 0) error("otp_dec: ERROR writing2 to socket\n");
			n = read(sockfd,result,255);
			if (n < 0) error("otp_dec: ERROR reading from socket\n");
			printf("%s", result);
			iter = 0;
		}
		if(ch2 == '\n') break;
		buffer[iter] = ch;
		buffer2[iter] = ch2;
		iter++;
		ch = getc(key);
		ch2 = getc(encrypted);
	}
	buffer[iter] = '\0';
	buffer2[iter] = '\0';
	n = write(sockfd,buffer,255);
	if (n < 0) error("otp_dec: ERROR writing1 to socket\n");
	n = write(sockfd,buffer2,255);
	if (n < 0) error("otp_dec: ERROR writing2 to socket\n");
	n = read(sockfd,result,255);
	if (n < 0) error("otp_dec: ERROR reading from socket\n");
	printf("%s\n", result);
	
	// Entire message printed, close socket and return.
    close(sockfd);
    return 0;
}