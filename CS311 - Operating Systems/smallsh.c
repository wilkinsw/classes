//Wilkins White - 931 838 790

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>

#define MAX_BUFFER	2048
#define MAX_INPUTS	512
#define MAX_PID		100
#define DEBUG		0

char buffer[MAX_BUFFER];
char *inputs[MAX_INPUTS];
pid_t pid_list[MAX_PID];
int pid_active[MAX_PID] = {0};
pid_t last_pid = 0;
int num_inputs = 0;

int background_flag = 0;
int output_flag = 0;
int input_flag = 0;

int last_status = -1;

void bgc();
void parse_input();
void handle_exit();
void handle_status();
void handle_cd();
void handle_execute(char program[], char *args[100]);
char *trim (char *);
int min3(int, int, int);

int main()
{
	int iter = 0;
	
	// Setting up the signal handler to ignore ^C
	struct sigaction act_ignore;
	act_ignore.sa_handler = SIG_IGN;
	sigaction(SIGINT, &act_ignore, NULL);
	

	while(1)
	{
		// Run background check and reset my flags for new input
		bgc();
		iter=0;
		background_flag = 0;
		output_flag = 0;
		input_flag = 0;
		
		// Print cursor and prompt for user input
		printf(": ");
		fgets(buffer, sizeof(buffer), stdin);
		
		// Get arguments as elements in an array
		inputs[iter] = strtok (buffer," ");
		while (inputs[iter] != NULL)
		{
			inputs[++iter] = strtok (NULL, " ");
		}
		num_inputs = iter-1;
		
		// Trim the trailing newline off the last arg
		inputs[num_inputs] = trim(inputs[num_inputs]);
		
		// Parse the input
		parse_input();
	}
    return(0);
}

void bgc()
{
	// Background check function
	int bg_status = 0;
	int iter = 0;
	pid_t finished_pid = -1;
	if(DEBUG) printf("FUNCTION: bgc()\n");
	
	//Iterate through all active processes and wait for them
	for(iter=0; iter<MAX_PID; iter++)
	{
		if(pid_active[iter] == 1)
		{
			finished_pid = waitpid(pid_list[iter], &bg_status, WNOHANG);
			if(finished_pid > 0)
			{
				if(WIFEXITED(bg_status))
				{
					printf("Process %d finished with code %d\n", finished_pid, WEXITSTATUS(bg_status));
				}
				else
				{
					printf("Process %d terminated by signal: %d\n", finished_pid, WTERMSIG(bg_status));
				}
				pid_active[iter] = 0;
			}
		}
	}
	
	

		
	
}

void parse_input()
{
	int a, b, c;
	int iter = 0;
	int split = 0;
	char *arg_list[100];
	
	if(DEBUG) printf ("FUNCTION: parse_input()\n");
	
	// If the input is a comment or a blank line just return
	if(strcmp(inputs[0], "\n") == 0) return;
	if(strcmp(inputs[0], "#") == 0) return;
	
	// Check to see if process should be run in background
	if(strcmp(inputs[num_inputs], "&") == 0) background_flag = num_inputs;
	
	// Locate any redirect symbols and record their positions
	for(iter=1; iter < num_inputs; iter++)
	{
		if(strcmp(inputs[iter], ">") == 0) output_flag = iter;
		if(strcmp(inputs[iter], "<") == 0) input_flag = iter;
	}
	
	if(DEBUG)
	{
		printf("& \t %d \n", background_flag);
		printf("> \t %d \n", output_flag);
		printf("< \t %d \n", input_flag);
	}		
	
	// Perform built in functions, else prepare to call exec
	if(strcmp(inputs[0], "exit") == 0) handle_exit();
	else if(strcmp(inputs[0], "status") == 0) handle_status();
	else if(strcmp(inputs[0], "cd") == 0) handle_cd();
	else
	{
		//Make sure zeros don't screw up the min3 function
		//I want the lowest number that's not a zero
		//So I know when the function arguments end
		if(input_flag) a = input_flag;
		else a = 999;
		if(output_flag) b = output_flag;
		else b = 999;
		if(background_flag) c = background_flag;
		else c = 999;
		
		split = min3(a, b, c);
		
		//If no flags were used just set the split to the max
		if(split==0 || split==999) split = num_inputs+1;
		
		//Iterate through the inputs and fill in the arg_list
		//This will cut out any symbols that shouldn't be sent to exec
		for(iter=0; iter < split; iter++)
		{
			arg_list[iter] = inputs[iter];
		}
		arg_list[iter] = NULL;
		
		//Send the first arg (command name) and arg list
		handle_execute(inputs[0], arg_list);
	}
}

void handle_exit()  //TODO
{
	int iter;
	
	//Iterate through all active processes and kill them off
	for(iter=0; iter<MAX_PID; iter++)
	{
		if(pid_active[iter] == 1)
		{
			kill(pid_list[iter], SIGTERM);
		}
	}
	exit(0);
}

void handle_status()
{
	if(DEBUG) printf ("FUNCTION: handle_status()\n");
	
	//Print the exit status of the last process
	if(WIFEXITED(last_status))
	{
		printf("exit value %d\n", WEXITSTATUS(last_status));
	}	
}

void handle_cd()
{
	char *HOME;
	char cwd[1024];
	
	if(DEBUG) printf ("FUNCTION: handle_cd()\n");
	
	if(inputs[1] == NULL) //If there is no input send us home
	{
		HOME = getenv("HOME");
		if(chdir(HOME) == 0)
		{
			if (getcwd(cwd, sizeof(cwd)) != NULL)
				fprintf(stdout, "%s\n", cwd);
			else
				perror("getcwd() error");
		}
		else
		{
			printf("Failed to switch directories");
		}
	}
	else if(chdir(inputs[1]) == 0) //Else go to the path
	{
		if (getcwd(cwd, sizeof(cwd)) != NULL)
			fprintf(stdout, "%s\n", cwd);
		else
			perror("getcwd() error");
	}
	else
	{
		printf("Failed to switch directories");
	}
}

void handle_execute(char program[], char *args[100])
{
	int fd_in, fd_out;
	int devNull = open("/dev/null", O_WRONLY);
	pid_t pid;
	int iter = 0;
	int status = 0;
	
	if(DEBUG) printf ("FUNCTION: handle_execute()\n");

	pid = fork();

	if (pid == -1)
	{
		// error, failed to fork()
		perror("Failed to fork... ");
		exit(1);
	}
	else if (pid == 0)
	{
		// child
		
		//Reset the sigaction to no-longer ignore interrupts
		struct sigaction act_default;
		act_default.sa_handler = SIG_DFL;
		sigaction(SIGINT, &act_default, NULL);
		
		if(DEBUG) 
		{
			printf ("Child executing %s with args:\n", program);
			for(iter=0; iter < 100; iter++)
			{
				if(args[iter] == NULL) break;
				printf("\t%s\n", args[iter]);
			}
			printf("\n");
		}
		
		//If this is a background process redirect output to null
		if(background_flag)
		{
			dup2(devNull, 1);
		}
		
		//Do input redirection if needed
		if(input_flag)
		{
			fd_in = open(inputs[input_flag+1], O_RDONLY);
			if(fd_in == -1)
			{
				perror("input open");
				exit(1);
			}	
			if(dup2(fd_in, 0) == -1)
			{
				perror("input dup2");
				exit(2);
			}
		}
		
		//Do output redirection if needed (overwrites background)
		if(output_flag)
		{
			fd_out = open(inputs[output_flag+1], O_WRONLY|O_CREAT|O_TRUNC, 0644);
			if(fd_out == -1)
			{
				perror("output open");
				exit(3);
			}	
			if(dup2(fd_out, 1) == -1)
			{
				perror("output dup2");
				exit(4);
			}
		}
			
		execvp(program, args);
		_exit(EXIT_FAILURE);   // exec never returns
	}
	else
	{
		// parent
		last_pid = pid;
		
		if(background_flag == 0)  //If this is not a background process
		{
			//Wait for function to finish
			waitpid(pid, &last_status, 0);
			//Notify user if it was terminated by a signal
			if(!WIFEXITED(last_status))
			{
				printf("terminated by signal: %d\n", WTERMSIG(last_status));
			}
		} 
		else //Else this is a background process
		{
			//Print out the pid
			printf("background pid is %d\n", pid);
			
			//Check array for an empty slot and insert new pid
			for(iter=0; iter<MAX_PID; iter++)
			{
				if(pid_active[iter] == 0)
				{
					pid_active[iter] = 1;
					pid_list[iter] = pid;
					break;
				}
			}
		}
	}
	
}

//Trims out newline from string
char *trim (char *s) {
	int i = strlen(s)-1;
	if ((i > 0) && (s[i] == '\n'))
		s[i] = '\0';
	return s;
}

//Takes minimum of 3 values
int min3(int a, int b, int c)
{
	if((a < b) && (a < c)) return a;
	else if((b < a) && (b < c)) return b;
	else if((c < a), (c < b)) return c;
	else return 0;
}