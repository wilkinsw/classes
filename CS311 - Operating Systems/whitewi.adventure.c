//Wilkins White - 931 838 790

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>

#define DEBUG 0

FILE *rm1, *rm2, *rm3, *rm4, *rm5, *rm6, *rm7;
char rms_dir[30];
char rm1_path[50];
char rm2_path[50];
char rm3_path[50];
char rm4_path[50];
char rm5_path[50];
char rm6_path[50];
char rm7_path[50];

char rm1_name[20];
char rm2_name[20];
char rm3_name[20];
char rm4_name[20];
char rm5_name[20];
char rm6_name[20];
char rm7_name[20];

int rm1_num_connections = 0;
int rm2_num_connections = 0;
int rm3_num_connections = 0;
int rm4_num_connections = 0;
int rm5_num_connections = 0;
int rm6_num_connections = 0;
int rm7_num_connections = 0;

int rm1_connection_key = 0;
int rm2_connection_key = 0;
int rm3_connection_key = 0;
int rm4_connection_key = 0;
int rm5_connection_key = 0;
int rm6_connection_key = 0;
int rm7_connection_key = 0;

void create_files();
void connect_rooms();
void rewind_files();
void play();
void victory();
void cleanup();
int check_finished();


int main()
{
	create_files();		//Chooses the rooms from the random list of 10 and creates the files
	connect_rooms();	//Forms the room connections
	play();				//Reads in the game data from the files and plays the game
	cleanup();			//Removes game files
    return(0);
}

void create_files()
{
	time_t t;						//Time value for seeding the RNG
	int roll = 0;					//Will store a random value 0-9
	int rooms = 0;					//Each room is encoded as a bit in this variable to check overlaps
	char *names[10];				//Holds the 10 room names for easy iteration
	char name0[] = "Study";
	char name1[] = "Kitchen";
	char name2[] = "Hall";
	char name3[] = "Conservatory";
	char name4[] = "Lounge";
	char name5[] = "Ballroom";
	char name6[] = "Dining_Room";
	char name7[] = "Library";
	char name8[] = "Billiard_Room";
	char name9[] = "Cellar";
	
	names[0] = name0;
	names[1] = name1;
	names[2] = name2;
	names[3] = name3;
	names[4] = name4;
	names[5] = name5;
	names[6] = name6;
	names[7] = name7;
	names[8] = name8;
	names[9] = name9;
	
	//Create the rooms directory with the PID
	if(DEBUG) printf("Creating...\n");
	sprintf(rms_dir, "./whitewi.rooms.%d", getpid());
	mkdir(rms_dir, 0700);
	if(DEBUG) printf("\t%s\n", rms_dir);
	
	srand((unsigned) time(&t));
	
	//Each while loop creates one file...
	while(1)
	{
		roll = rand()%10;			//Number 0-9 is rolled
		if((rooms >> roll) & 1)		//Check to see if this room already exists
		{
			continue;
		}
		else
		{
			rooms += (1 << roll);										//Encode this room value to prevent overlap
			sprintf(rm1_path, "%s/%s.room", rms_dir, names[roll]);		//Create the file path string
			rm1 = fopen(rm1_path, "w+");								//Create the file and store the path
			fprintf(rm1, "ROOM NAME: %s\n", names[roll]);				//Write the room name to the file
			sprintf(rm1_name, "%s", names[roll]);						//Write the room name to the name global variable
			if(DEBUG) printf("\t%s\n", rm1_path);
			break;
		}
	}
	
	//Subsequent while loops are copies of the first.
	while(1)
	{
		roll = rand()%10;
		if((rooms >> roll) & 1)
		{
			continue;
		}
		else
		{
			rooms += (1 << roll);
			sprintf(rm2_path, "%s/%s.room", rms_dir, names[roll]);
			rm2 = fopen(rm2_path, "w+");
			fprintf(rm2, "ROOM NAME: %s\n", names[roll]);
			sprintf(rm2_name, "%s", names[roll]);
			if(DEBUG) printf("\t%s\n", rm2_path);
			break;
		}
	}
	while(1)
	{
		roll = rand()%10;
		if((rooms >> roll) & 1)
		{
			continue;
		}
		else
		{
			rooms += (1 << roll);
			sprintf(rm3_path, "%s/%s.room", rms_dir, names[roll]);
			rm3 = fopen(rm3_path, "w+");
			fprintf(rm3, "ROOM NAME: %s\n", names[roll]);
			sprintf(rm3_name, "%s", names[roll]);
			if(DEBUG) printf("\t%s\n", rm3_path);
			break;
		}
	}
	while(1)
	{
		roll = rand()%10;
		if((rooms >> roll) & 1)
		{
			continue;
		}
		else
		{
			rooms += (1 << roll);
			sprintf(rm4_path, "%s/%s.room", rms_dir, names[roll]);
			rm4 = fopen(rm4_path, "w+");
			fprintf(rm4, "ROOM NAME: %s\n", names[roll]);
			sprintf(rm4_name, "%s", names[roll]);
			if(DEBUG) printf("\t%s\n", rm4_path);
			break;
		}
	}
	while(1)
	{
		roll = rand()%10;
		if((rooms >> roll) & 1)
		{
			continue;
		}
		else
		{
			rooms += (1 << roll);
			sprintf(rm5_path, "%s/%s.room", rms_dir, names[roll]);
			rm5 = fopen(rm5_path, "w+");
			fprintf(rm5, "ROOM NAME: %s\n", names[roll]);
			sprintf(rm5_name, "%s", names[roll]);
			if(DEBUG) printf("\t%s\n", rm5_path);
			break;
		}
	}
	while(1)
	{
		roll = rand()%10;
		if((rooms >> roll) & 1)
		{
			continue;
		}
		else
		{
			rooms += (1 << roll);
			sprintf(rm6_path, "%s/%s.room", rms_dir, names[roll]);
			rm6 = fopen(rm6_path, "w+");
			fprintf(rm6, "ROOM NAME: %s\n", names[roll]);
			sprintf(rm6_name, "%s", names[roll]);
			if(DEBUG) printf("\t%s\n", rm6_path);
			break;
		}
	}
	while(1)
	{
		roll = rand()%10;
		if((rooms >> roll) & 1)
		{
			continue;
		}
		else
		{
			rooms += (1 << roll);
			sprintf(rm7_path, "%s/%s.room", rms_dir, names[roll]);
			rm7 = fopen(rm7_path, "w+");
			fprintf(rm7, "ROOM NAME: %s\n", names[roll]);
			sprintf(rm7_name, "%s", names[roll]);
			if(DEBUG) printf("\t%s\n", rm7_path);
			break;
		}
	}
}

void connect_rooms()
{
	int iter = 0;
	int A, B;					//Hold the roll room numbers
	int start, end;				//Hold the start and end room numbers
	FILE *rooms[7];				//Hold the file pointers for easy indexing	
	char *names[7];				//Hold the room names for easy indexing
	int *num_connections[7];	//Hold the number of connections for easy indexing
	int *keys[7];				//Hold the encoded keys for easy indexing
	
	rooms[0] = rm1;
	rooms[1] = rm2;
	rooms[2] = rm3;
	rooms[3] = rm4;
	rooms[4] = rm5;
	rooms[5] = rm6;
	rooms[6] = rm7;
	
	names[0] = rm1_name;
	names[1] = rm2_name;
	names[2] = rm3_name;
	names[3] = rm4_name;
	names[4] = rm5_name;
	names[5] = rm6_name;
	names[6] = rm7_name;
	
	num_connections[0] = &rm1_num_connections;
	num_connections[1] = &rm2_num_connections;
	num_connections[2] = &rm3_num_connections;
	num_connections[3] = &rm4_num_connections;
	num_connections[4] = &rm5_num_connections;
	num_connections[5] = &rm6_num_connections;
	num_connections[6] = &rm7_num_connections;
	
	keys[0] = &rm1_connection_key;
	keys[1] = &rm2_connection_key;
	keys[2] = &rm3_connection_key;
	keys[3] = &rm4_connection_key;
	keys[4] = &rm5_connection_key;
	keys[5] = &rm6_connection_key;
	keys[6] = &rm7_connection_key;
	
	if(DEBUG) printf("\nCONNECTING ROOMS:\n");
	while(1)
	{
		if(check_finished()) break;		//If all rooms are done, break;
		
		A = rand()%7;		//Choose a random first room
		B = rand()%7;		//Choose a random second room
		
		if(DEBUG) printf("Rolled %d & %d\n", A, B);
		
		if(A == B) continue;	//Try again if the rooms are the same
		
		if ((*num_connections[A] >= 6) || (*num_connections[B] >= 6)) continue;	//Try again if a room has too many connections
		if((*keys[A] >> B) & 1) continue;										//Try again if the rooms are already connected
		
		//Increment number of connections
		*num_connections[A] = *num_connections[A] + 1;
		*num_connections[B] = *num_connections[B] + 1;
		
		//Connect the two rooms and encode the other room value in the key to avoid repeats.
		fprintf(rooms[A], "CONNECTION %d: %s\n", *num_connections[A], names[B]);
		if(DEBUG) printf("CONNECTION %d: %s\n", *num_connections[A], names[B]);
		*keys[A] = *keys[A] | (1 << B);
		
		fprintf(rooms[B], "CONNECTION %d: %s\n", *num_connections[B], names[A]);
		if(DEBUG) printf("CONNECTION %d: %s\n", *num_connections[B], names[A]);
		*keys[B] = *keys[B] | (1 << A);
	}
	
	//Randomize which room is the start room and which is the end room
	while(1)
	{
		start = rand()%7;
		end = rand()%7;
		
		if(start == end) continue;
		else break;
	}
	for(iter=0; iter < 7; iter++)
	{
		if(iter == start) fprintf(rooms[iter], "ROOM TYPE: START_ROOM\n");
		else if(iter == end) fprintf(rooms[iter], "ROOM TYPE: END_ROOM\n");
		else fprintf(rooms[iter], "ROOM TYPE: MID_ROOM\n");
	}
	
}

void rewind_files()
{
	//Rewinds files to start reading at first line.
	if(DEBUG) printf("\nREWINDING FILES:\n");

	rewind(rm1);
	rewind(rm2);
	rewind(rm3);
	rewind(rm4);
	rewind(rm5);
	rewind(rm6);
	rewind(rm7);
}

void play()
{
	char path[1000];						//Contains the path taken by the player
	int steps = 0;							//Contains the number of steps taken by the player
	int location = 0;						//Contains the player's current location
	int start = 0;							//Contains the start location
	int end = 0;							//Contains the end (goal) location
	int first_run = 1;						//First run flag
	int bad_input = 1;						//Bad input flag
	int iter, iter2 = 0;					
	char buffer[256];
	char str1[20], str2[20], str3[20];
	FILE *rooms[7];
	
	char *names[7];
	char name1[20];
	char name2[20];
	char name3[20];
	char name4[20];
	char name5[20];
	char name6[20];
	char name7[20];
	
	int *keys[7];
	int key1 = 0;
	int key2 = 0;
	int key3 = 0;
	int key4 = 0;
	int key5 = 0;
	int key6 = 0;
	int key7 = 0;

	
	char *connections[7];
	char connections1[100];
	char connections2[100];
	char connections3[100];
	char connections4[100];
	char connections5[100];
	char connections6[100];
	char connections7[100];
	
	rooms[0] = rm1;
	rooms[1] = rm2;
	rooms[2] = rm3;
	rooms[3] = rm4;
	rooms[4] = rm5;
	rooms[5] = rm6;
	rooms[6] = rm7;
	
	names[0] = name1;
	names[1] = name2;
	names[2] = name3;
	names[3] = name4;
	names[4] = name5;
	names[5] = name6;
	names[6] = name7;
	
	keys[0] = &key1;
	keys[1] = &key2;
	keys[2] = &key3;
	keys[3] = &key4;
	keys[4] = &key5;
	keys[5] = &key6;
	keys[6] = &key7;
	
	connections[0] = connections1;
	connections[1] = connections2;
	connections[2] = connections3;
	connections[3] = connections4;
	connections[4] = connections5;
	connections[5] = connections6;
	connections[6] = connections7;
	
	printf("\nStarting Game.\n\n");
	
	//Rewind files to restart at line 1 for reading
	rewind_files();
	
	//First iterate through all files and fill in the names array.  Also identify room types.
	//Must be done before connection list can be populated.
	for(iter=0; iter < 7; iter++)
	{
		//Read file line by line into buffer
		while (fgets (buffer, sizeof(buffer), rooms[iter]))
		{
			//Scan words into seperate strings for analysis
			sscanf(buffer, "%s %s %s", str1, str2, str3);
			
			//Fill in the name array and determine the start and end locations
			if(strcmp(str2, "NAME:") == 0)
			{
				if(DEBUG) printf("Setting name%d to %s\n", iter, str3);
				strcpy(names[iter], str3);
			}
			else if(strcmp(str2, "TYPE:") == 0)
			{
				if(strcmp(str3, "START_ROOM") == 0)
				{
					if(DEBUG) printf("Setting START to %d\n", iter);
					start = iter;
				}
				else if(strcmp(str3, "END_ROOM") == 0)
				{
					if(DEBUG) printf("Setting END to %d\n", iter);
					end = iter;
				}
			}
		}
	}
	
	//Rewind files to restart at line 1 for reading
	rewind_files();
	
	//Iterate through each file again and fill in the connection list.  Encode connections in a key.
	for(iter=0; iter < 7; iter++)
	{
		first_run = 1;
		//Read the file line by line
		while (fgets (buffer, sizeof(buffer), rooms[iter]))
		{
			//Scan words into seperate strings for analysis
			sscanf(buffer, "%s %s %s", str1, str2, str3);
			
			//Find each connection line
			if(strcmp(str1, "CONNECTION") == 0)
			{	
				//Locate which name the connection corresponds to and encode that value in the key
				//This is why name array had to be populated first.
				for(iter2=0; iter2 < 7; iter2++)
				{
					if(DEBUG) printf("Comparing %s to %s\n", str3, names[iter2]);
					if(strcmp(str3, names[iter2]) == 0)
					{
						if(DEBUG) printf("%x | %x = %x\n", (*keys[iter]), (1 << iter2), ((*keys[iter]) | (1 << iter2)));
						(*keys[iter]) = (*keys[iter]) | (1 << iter2);
						break;
					}
				}
				
				//Copy the name into the connections string.
				if(first_run)
				{
					//If this is the first time then copy the name in
					strcpy(connections[iter], str3);
					first_run = 0;
				}
				else
				{
					//Else add punctuation and append to the end of the string
					sprintf(connections[iter], "%s, %s", connections[iter], str3);
				}
			}
		}
	}
	
	//Debug info
	if(DEBUG)
	{
		printf("START:\t%d\n", start);
		printf("END:\t%d\n", end);
		printf("NAMES:\n");
		for(iter=0; iter < 7; iter++)
		{
			printf("\tname%d\t%s\n", iter, names[iter]);
		}
		printf("KEYS:\n");
		for(iter=0; iter < 7; iter++)
		{
			printf("\tkey%d\t%x\n", iter, *keys[iter]);
		}
		printf("CONNECTIONS:\n");
		for(iter=0; iter < 7; iter++)
		{
			printf("\tconnections%d\t%s\n", iter, connections[iter]);
		}
	}
	
	//Play the game
	location = start;
	strcpy(path, "");
	while(1)
	{
		//Display current location and prompt for player input.
		printf("CURRENT LOCATION: %s\n", names[location]);
		printf("POSSIBLE CONNECTIONS: %s.\n", connections[location]);
		printf("WHERE TO?>");
		scanf("%s", buffer);
		bad_input = 1;
		
		//Check the input for validity by comparing buffer to names list and checking key for valid move.
		for(iter=0; iter < 7; iter++)
		{
			if(DEBUG) printf("Comparing %s to %s\n", buffer, names[iter]);
			if(strcmp(buffer, names[iter]) == 0)
			{
				if(DEBUG) printf("%x >> %d & 1 = %d\n", *keys[location], iter, ((*keys[location] >> iter) & 1));
				if((*keys[location] >> iter) & 1) //Each valid move is encoded as a bit in the key for the room
				{
					location = iter;									//Change location to the new location
					printf("MOVING TO: %s\n\n", names[location]);		//Alert the user to a successful move
					sprintf(path, "%s\n%s", path, names[location]);		//Add the new room to the path
					steps++;											//Increment steps taken
					bad_input = 0;										//Flag that input was valid
				}
				break;
			}
		}
		if(bad_input) printf("\nHUH? I DON'T UNDERSTAND THAT ROOM. TRY AGAIN.\n\n"); //Yell at the user if they gave a bad input.
		
		//Check to see if end location was reached.
		if(location == end) break;
	}
	printf("YOU HAVE FOUND THE END ROOM. CONGRATULATIONS!\n");
	printf("YOU TOOK %d STEPS. YOUR PATH TO VICTORY WAS:", steps);
	printf("%s\n\n", path);
	victory(names[end]);
}

void victory(char *room)
{
	//This function provides randomized flavor text upon winning based on Clue board game.
	int a = 0;
	int b = 0;
	
	char *players[6];
	char player0[20] = "Prof. Plum";
	char player1[20] = "Mrs. White";
	char player2[20] = "Mr. Green";
	char player3[20] = "Mrs. Peacock";
	char player4[20] = "Miss Scarlett";
	char player5[20] = "Col. Mustard";
	
	char *weapons[6];
	char weapon0[20] = "Candlestick";
	char weapon1[20] = "Wrench";
	char weapon2[20] = "Rope";
	char weapon3[20] = "Revolver";
	char weapon4[20] = "Knife";
	char weapon5[20] = "Lead Pipe";
	
	players[0] = player0;
	players[1] = player1;
	players[2] = player2;
	players[3] = player3;
	players[4] = player4;
	players[5] = player5;
	
	weapons[0] = weapon0;
	weapons[1] = weapon1;
	weapons[2] = weapon2;
	weapons[3] = weapon3;
	weapons[4] = weapon4;
	weapons[5] = weapon5;
	
	a = rand()%6;
	b = rand()%6;
	
	printf("It was %s in the %s with the %s!\n", players[a], room, weapons[b]);
}

void cleanup()
{	
	//Closes all file pointers and removes the files.
	if(DEBUG) printf("Removing...\n");
	if(DEBUG) printf("\t%s\n", rm1_path);
	fclose(rm1);
	remove(rm1_path);
	if(DEBUG) printf("\t%s\n", rm2_path);
	fclose(rm2);
	remove(rm2_path);
	if(DEBUG) printf("\t%s\n", rm3_path);
	fclose(rm3);
	remove(rm3_path);
	if(DEBUG) printf("\t%s\n", rm4_path);
	fclose(rm4);
	remove(rm4_path);
	if(DEBUG) printf("\t%s\n", rm5_path);
	fclose(rm5);
	remove(rm5_path);
	if(DEBUG) printf("\t%s\n", rm6_path);
	fclose(rm6);
	remove(rm6_path);
	if(DEBUG) printf("\t%s\n", rm7_path);
	fclose(rm7);
	remove(rm7_path);
	//Remove empty directory.
	if(DEBUG) printf("\t%s\n", rms_dir);
	rmdir(rms_dir);
}

int check_finished()
{
	//Checks to see if rooms have enough connections
	if		(rm1_num_connections < 3) 	return(0);
	else if	(rm2_num_connections < 3)	return(0);
	else if	(rm3_num_connections < 3)	return(0);
	else if	(rm4_num_connections < 3)	return(0);
	else if	(rm5_num_connections < 3)	return(0);
	else if	(rm6_num_connections < 3)	return(0);
	else if	(rm7_num_connections < 3)	return(0);
	else							return(1);
}